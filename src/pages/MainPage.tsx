import * as React from "react";
import { DatabaseOverviewPage } from "./OverviewPage";
import * as Model from "../model/Model";
import * as Router from "react-router";
import { Hello } from "../components/Hello"
import { AppFrame } from "../components/AppFrame";
import { AppFrameCovid } from "../components/AppFrameCovid";
import { DatabaseTable } from "../components/DatabaseTable"
import { DatabasePage } from "./DatabasePage"
import { RecordDetails } from "../components/RecordDetails";
import { LocationArgs } from "../model/Application";

let filterPrefix = "filter_"

function updateLocation(a: LocationArgs) {
    let href = "#/records?";
    var firstParam = true
    if(a.recordsPage != null) {
        href += "page=" + Math.floor(a.recordsPage);
        firstParam = false;
    }
    if(a.filters != null) {
        for(let name in a.filters) {
            if(!firstParam) {
                href += "&"
            }
            href += filterPrefix + name + "=" + encodeURIComponent(a.filters[name])
            firstParam = false;
        }
    }
    window.location.href = href;
}

function parseLocation(rawQueryObj: Object): LocationArgs {
    let query = rawQueryObj as {[key:string]:string};
    
    let page: number = null
    if(query["page"] != null) {
        page = Number(query["page"])
    }

    let filters: {[name:string]:string} = {}
    for(let q in query) {
        if(q.substring(0, filterPrefix.length) === filterPrefix) {
            let name = q.slice(filterPrefix.length);
            filters[name] = query[q];
        }
    }
    

    return {
        recordsPage: page,
        filters: filters
    }
}



export interface MainPageProps {
    readonly database: Model.MarDatabase 
}

export interface MainPageState {
    selectedRecordsMmpId: Set<string>,
    selectedFileType:  {[key:string]:boolean}
}

export class MainPage extends React.Component<MainPageProps, MainPageState> {
    constructor(props: MainPageProps) {
        super(props)
        this.state = {
            selectedRecordsMmpId: new Set(),
            selectedFileType:{
                "tsv": true,
                "fasta": false,
                "xml": false,
                "Genbank": false
            }
        }
        this.onSelectRecord = this.onSelectRecord.bind(this);
        this.onSelectFileType = this.onSelectFileType.bind(this);
        this.unselectAll = this.unselectAll.bind(this);
    }

    onSelectRecord(mmpID: string, selected: boolean) {
        let selectedRecordsNew = this.state.selectedRecordsMmpId
        if(selected) {
            selectedRecordsNew.add(mmpID)
        }
        else if(selectedRecordsNew.has(mmpID)) {
            selectedRecordsNew.delete(mmpID)
        }

        this.setState({
            selectedRecordsMmpId: selectedRecordsNew,
            selectedFileType: this.state.selectedFileType
        });
    }

    onSelectFileType(fileType: string) {
        let selectedFileType: {[key:string]:boolean} = {};
        for(let key in this.state.selectedFileType) {
            selectedFileType[key] = false;
        }

        selectedFileType[fileType] = true;

        this.setState({
            selectedRecordsMmpId: this.state.selectedRecordsMmpId,
            selectedFileType: selectedFileType
        });
    }

    unselectAll() {
        this.setState({
            selectedRecordsMmpId: new Set(),
            selectedFileType: this.state.selectedFileType
        });
    }

    render() {
        let AF = ""
        let db = this.props.database;
        let attributeOrder = db.defaultAttributesOrder;

        if(db.type == "sarscov2db")
        {
            AF = AppFrameCovid
        }
        else
        {
            AF = AppFrame
        }
    
        function onSelectPage(page: number, filters: {[name:string]:string}) {
            updateLocation({
                recordsPage: page,
                filters: filters
            })
        }

        function onSelectFilters(filters: {[name:string]:string}) {
            updateLocation({
                filters: filters
            })
        }
    
        return (
            <Router.Router history={Router.hashHistory}>
                <Router.Route path="/" component={AF}>
                    <Router.IndexRoute component={() => (<DatabaseOverviewPage database={db}/>)}/>
                    <Router.Route path="/records">
                        <Router.IndexRoute component={(r: Router.RouteComponentProps<any, any>) =>
                            (<DatabasePage
                                database={db}
                                locationArgs={parseLocation(r.location.query)}
                                //locationArgs={{}}
                                //query={r.location.query}
                                onSelectRecord={this.onSelectRecord}
                                onSelectFileType={this.onSelectFileType}
                                onSelectFilters={onSelectFilters}
                                onUnselectAll={this.unselectAll}
                                selectedRecords={this.state.selectedRecordsMmpId}
                                selectedFileType={this.state.selectedFileType}
                                onSelectPage={onSelectPage}/>)}/>
                        <Router.Route path=":record" component={(r: Router.RouteComponentProps<any, any>) =>
                            (<RecordDetails record={db.recordsById[r.params["record"]]} databaseType={db.type} recordId={r.params["record"]}/>)}/>
                    </Router.Route>
                </Router.Route>
            </Router.Router>
        );
    }
}