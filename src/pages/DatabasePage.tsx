import * as React from "react";
import { DatabaseOverviewPage } from "./OverviewPage";
import { DatabaseTable } from "../components/DatabaseTable";
import * as Model from "../model/Model";
import { Pagination } from "react-bootstrap-typescript";
import * as Orderings from "../utils/Orderings";
import { SelectedRecordsOptions } from "../components/SelectedRecordsOptions";
import { DropDownPanel } from "../components/DropDownPanel";
import { FilterPanel, Filters as FilterPanelFilters } from "../components/FilterPanel";
import { LocationArgs } from "../model/Application";
import * as Filters from "../utils/Filters";
import { FilterTextSearch } from "../components/FilterTextSearch";
import * as $ from "jquery"

export interface DatabasePageProps {
    readonly database: Model.MarDatabase
    locationArgs: LocationArgs
    onSelectPage: (page: number, filters: {[name:string]:string}) => void
    onSelectRecord: (mmpID: string, selected: boolean) => void
    onSelectFileType: (fileType: string) => void
    onSelectFilters: (filters: {[name:string]:string}) => void
    onUnselectAll: () => void
    selectedRecords: Set<string>,
    selectedFileType: {[key:string]:boolean}
    recordsPerPage?: number
}

export interface DatabasePageState {
    dropdowns: {[name:string]:boolean}
}

export class DatabasePage extends React.Component<DatabasePageProps, DatabasePageState> {
    constructor() {
        super();
        this.state = {
            dropdowns: {
                "selectedRecords": false,
                "columns": true,
                "filter": false,
                "search": false
            }
        }
        this.selectedRecords = this.selectedRecords.bind(this);
        this.toggleDropDown = this.toggleDropDown.bind(this);
        this.unselectAll = this.unselectAll.bind(this);
    }

    toggleDropDown(name: string) {
        let dropdowns: {[name:string]:boolean} = {}
        for(let n in this.state.dropdowns) {
            dropdowns[n] = this.state.dropdowns[n];
        }
        dropdowns[name] = !dropdowns[name];
        this.setState({
            dropdowns: dropdowns
        })
    }

    selectedRecords(): Array<Model.MarRecord> {
        let records: Array<Model.MarRecord> = []
        for(let id in this.props.selectedRecords) {
            records.push(this.props.database.recordsById[id]);
        }

        return records;
    }

    unselectAll() {
        this.props.onUnselectAll();
    }

    render() {
        //console.log(this.props.locationArgs);

        /////////// begin filters

        // Filtering
        let stringFilterAttrNames: Array<string> = [];
        let rangeFilterAttrNames: Array<string> = [];
        

        let filters: Array<Filters.RecordFilter> = [];

        let filterQuery = this.props.locationArgs.filters || {};

        function stringFilter(attrName: string): Filters.StringFilterParams {
            stringFilterAttrNames.push(attrName);
            let filterParams: Filters.StringFilterParams = null;
            if(filterQuery[attrName] != null) {
                filterParams = {
                    equals: filterQuery[attrName]
                }
            }
            if(filterParams != null) {
                let filter = Filters.RecordFilter(Filters.StringFilter, filterParams, attrName)
                filters.push(filter);
            }
            return filterParams;
        }
        
        function stringFilterStringList(attrName: string): Filters.StringFilterStringList {
            stringFilterAttrNames.push(attrName);
            let filterParams: Filters.StringFilterStringList = null;
            if(filterQuery[attrName] != null) {
                filterParams = {
                    equals: filterQuery[attrName]
                }
            }
            if(filterParams != null) {
                let filter = Filters.RecordFilter(Filters.StringFilterStringList, filterParams, attrName)
                filters.push(filter);
            }
            return filterParams;
        }

        function textFilter(): Filters.TextFilterParams {
            let query = filterQuery["text"];

            if(query == null) {
                query = "";
            }

            let filterParams: Filters.TextFilterParams = {
                query: query
            };

            let textFilter = Filters.TextFilter(filterParams);
            if(query != "") {
                filters.push(textFilter);
            }

            return filterParams;
        }

        function rangeFilter(attrName: string, filter: Filters.AttributeFilter<Filters.RangeFilterParams>): Filters.RangeFilterParams {
            rangeFilterAttrNames.push(attrName);
            let minQName = attrName.concat("_min");
            let maxQName = attrName.concat("_max");
            
            let filterParams: Filters.RangeFilterParams = {
                min: -Infinity,
                max: Infinity
            }
            
            if(filterQuery[minQName] != null) {
                filterParams.min = Number(filterQuery[minQName]);
            }

            if(filterQuery[maxQName] != null) {
                filterParams.max = Number(filterQuery[maxQName]);
            }
            
            let recordFilter = Filters.RecordFilter(filter, filterParams, attrName);

            if((filterParams.min !== -Infinity) || (filterParams.max !== Infinity)) {
                filters.push(recordFilter);
            }

            return filterParams;
        }


        let filterParams: FilterPanelFilters = {
            textSearch: textFilter(),
            projectName: stringFilter("projectName"),
            envPackage: stringFilter("envPackage"),
            envBiome: stringFilter("envBiome"),
            envFeature: stringFilter("envFeature"),
            envMaterial: stringFilter("envMaterial"),
            isolationCountry: stringFilter("isolationCountry"),
            phylum: stringFilter("phylum"),
            family: stringFilter("family"),
            genus: stringFilter("genus"),
            analysisProjectType: stringFilter("analysisProjectType"),
            hostScientificName: stringFilter("hostScientificName"),
            oxygenRequirement: stringFilter("oxygenRequirement"),
            temperatureRange: stringFilter("temperatureRange"),
            halotolerance: stringFilter("halotolerance"),
            collectionDate: rangeFilter("collectionDate", Filters.YearFilter),
            depth: rangeFilter("depth", Filters.PrecicionDecimalStringFilter),
            antismashTypes: stringFilterStringList("antismashTypes"),
            chebiName: stringFilterStringList("chebiName"),
            // Covid filters
            hostSex: stringFilter("hostSex"),
            bodySampleSite: stringFilter("bodySampleSite"),
            sampleType: stringFilter("sampleType"),
            isolationSource: stringFilter("isolationSource"),
            //hostAge: rangeFilter("hostAge", Filters.PrecicionDecimalStringFilter),
        }

        let filteredRecords: Array<Model.MarRecord> = Filters.FilterRecords(filters, this.props.database.records);

        // Callbacks

        let onFiltersChange = ((filters: FilterPanelFilters) => {
            let newFiltersQuery: {[name:string]:string} = {}

            // String filters
            for(let attrName of stringFilterAttrNames) {
                let filterParams = filters[attrName] as Filters.StringFilterParams;
                if(filterParams != null) {
                    newFiltersQuery[attrName] = filterParams.equals;
                }
            }

            // Range filters
            for(let attrName of rangeFilterAttrNames) {
                let filterParams = filters[attrName] as Filters.RangeFilterParams;
                if(filterParams != null) {
                    let minQName = attrName.concat("_min");
                    let maxQName = attrName.concat("_max");
                    newFiltersQuery[minQName] = filterParams.min.toString();
                    newFiltersQuery[maxQName] = filterParams.max.toString();
                }
            }

            if(filters.textSearch != null) {
                let textQuery = filters.textSearch.query;
                if(textQuery != null && textQuery != "") {
                    newFiltersQuery["text"] = textQuery;
                }
            }

            // TODO: other filters...

            this.props.onSelectFilters(newFiltersQuery);
        }).bind(this);        

        let onSelectPage = ((page: number) => {
            this.props.onSelectPage(page, filterQuery);
        }).bind(this);

        let textQuery = "";
        if(filterParams.textSearch != null) {
            textQuery = filterParams.textSearch.query;
        }

        let onTextSearch = ((query: string) => {
            let newFilters: FilterPanelFilters = {} as FilterPanelFilters;
            $.extend(newFilters, filterParams);
            newFilters.textSearch = {
                query: query
            };
            onFiltersChange(newFilters)
        }).bind(this);

        let filterTextSearch = <FilterTextSearch query={textQuery} onSearch={onTextSearch}/>

        /////////// end filters

        let page = this.props.locationArgs.recordsPage || 1

        let recordsPerPage = this.props.recordsPerPage || 25; // 25

        let visibleRecords: Array<Model.MarRecord> = [];
        let firstRecord = recordsPerPage * (page - 1);
        let lastRecord = firstRecord + recordsPerPage - 1;
        let lastDatabaseRecord = filteredRecords.length - 1;
        if(lastRecord > lastDatabaseRecord) {
            lastRecord = lastDatabaseRecord;
        }
        for(let i=firstRecord; i <= lastRecord; i++) {
            visibleRecords.push(filteredRecords[i]);
        }

        let numFilteredRecords = filteredRecords.length;
        let numberOfPages = Math.ceil(numFilteredRecords / recordsPerPage)

        let paginator =
            <Pagination
                prev
                next
                first
                last
                ellipsis
                items={numberOfPages}
                maxButtons={10}
                activePage={page}
                onSelect={onSelectPage}
            />

        return (
            <div>
                <div className="row">
                    <div className="col-md-2" />
                    <div className="col-md-6">
                        {paginator}
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-3">
                        <DropDownPanel heading="Download selected records"
                            collapsed={this.state.dropdowns["selectedRecords"]}
                            onToggle={() => this.toggleDropDown("selectedRecords") }>
                            <SelectedRecordsOptions
                                records={this.props.selectedRecords}
                                //fileType={this.props.selectedFileType} // Currently, only TSV is implemented. Hide the database file types download buttons until they are implemented.
                                onSelectFileType={this.props.onSelectFileType}
                                onUnselectAll={this.unselectAll}
                                databaseType={this.props.database.type}/>
                        </DropDownPanel>
                        <DropDownPanel heading="Search"
                            collapsed={this.state.dropdowns["search"]}
                            onToggle={() => this.toggleDropDown("search") }>
                            {filterTextSearch}
                        </DropDownPanel>
                        <DropDownPanel heading="Filter"
                            collapsed={this.state.dropdowns["filter"]}
                            onToggle={() => this.toggleDropDown("filter") }>
                            <FilterPanel
                                database={this.props.database}
                                records={filteredRecords}
                                filters={filterParams}
                                numFilteredRecords={numFilteredRecords}
                                onFiltersChange={onFiltersChange}/>
                        </DropDownPanel>
                    </div>
                    <div className="col-md-5">
                        <DatabaseTable
                                attributeOrder={Orderings.TableColumns(this.props.database.type)}
                                attributeSize={Orderings.AttributeCompactSize}
                                records={visibleRecords}
                                selectedRecords={this.props.selectedRecords}
                                onSelectChange={this.props.onSelectRecord}/>
                    </div>
                </div>
            </div>
        );

        /*
                        <DropDownPanel heading="Selected records"
                            collapsed={this.state.dropdowns["selectedRecords"]}
                            onToggle={() => this.toggleDropDown("selectedRecords") }>
                            <SelectedRecordsOptions records={this.selectedRecords()} onUnselectAll={this.unselectAll}/>
                        </DropDownPanel>

                        <DropDownPanel heading="Visible attributes"
                            collapsed={this.state.dropdowns["columns"]}
                            onToggle={() => this.toggleDropDown("columns") }>
                            <p>Hello, World!</p>
                        </DropDownPanel>
         */
    }
}