import * as React from "react";
import * as Model from "../model/Model"
import * as Statistics from "../utils/Statistics"
import { TablePanel } from "../components/TablePanel"

export interface DatabaseOverviewProps { database: Model.MarDatabase }

export class DatabaseOverviewPage extends React.Component<DatabaseOverviewProps, {}> {
    render() {
        let stats = Statistics.DatabaseStatistics(this.props.database);
        let investigationTypes: Array<[string, string]> = []
        /*
        let keys = Object.keys(stats.investigationType);
        keys.sort
        for(let key of keys) {
            investigationTypes.push([key, stats.investigationType[key].toString()]);
        }
        */
        investigationTypes.push(["Total number of records", stats.numRecords.toString()])

        let mapsHref: string = "";
        switch (this.props.database.type) {
            case "marref":
                mapsHref = "https://www.google.com/maps/d/u/0/embed?mid=1SxN51Rmg3AVJG-B-1jJwMFF9pjfzWE4l&z=1";
                break;
            case "mardb":
                mapsHref = "https://www.google.com/maps/d/u/0/embed?mid=16WSRRWRupGRZgimLKIPz9YRwZ6sy3g3b&z=1";
                break;
            case "marcat":
                mapsHref = "https://www.google.com/maps/d/u/0/embed?mid=1SqVs1OvhiCQDoWM7rgyaqIoXdAqLAL0t&z=1";
                break;
            case "marfun":
                mapsHref = "https://www.google.com/maps/d/embed?mid=1_-y7jGNnVC2BVCIlArKM8CzccSwKygIt&z=1";
                break;
            case "saldb":
                mapsHref = "https://www.google.com/maps/d/embed?mid=13j2gl7glPMOCfuJpdeB1RyWrjvsXVXua&z=1";
                break;
        }

        return (
            <div className="container">
                <TablePanel
                    heading="Database overview"
                    elements={investigationTypes}
                />
                <iframe src={mapsHref} width="100%" height="500em"></iframe>
            </div>
        );
    }
}

