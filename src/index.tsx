import * as React from "react";
import * as ReactDOM from "react-dom";

import { Hello } from "./components/Hello";
import { MainPage } from "./pages/MainPage";

import * as $ from "jquery"
import * as Api from "./utils/Api"

import { parseXSD } from './integration/Xml'

Api.GetDatabase().then(database => {
    ReactDOM.render(
        <MainPage database={database}/>,
        document.getElementById("app")
    );
})
