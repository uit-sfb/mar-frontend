import { AttributeType, MarRecord, MarDatabase, Attribute, Missing, String } from "../model/Model";
import * as Model from "../model/Model";
import * as $ from "jquery"

import * as Xml2O from 'xml2o';

let attributeTypeMappings: {[key:string]:(attributeName: string, e: Object) => Attribute} = {
    "xs:string": XString,
    "xs:date": XString,
    "xs:integer": XString,
    "xs:positiveInteger": XString,
    "xs:nonNegativeInteger": XString,
    "xs:boolean": XBoolean,
    "Missing": XMissing,
    "AnalysisProjectType": XString,
    "EnvPackage": XString,
    "InvestigationType": XString,
    "Rrnas": XRrnas,
    "GramStain": XString,
    "CellShape": XString,
    "TemperatureRange": XString,
    "Halotolerance": XString,
    "OxygenRequirement": XString,
    "OtherTyping": XOtherTyping,
    "GenomeStatus": XString,
    "HostSex": XString,
    "PrecisionDecimalString": XPrecisionDecimalString,
    "PrecisionDecimalStringRange": XPrecisionDecimalStringRange,
    "PrecisionDecimalStringFixed": XPrecisionDecimalStringFixed,
    "Duration": XDuration,
    "DurationUnit": XString,
    "Position": XPosition,
    "DecimalString": XString,
    "AccessionList": XAccessionList,
    "PublicationPmid": XPublicationPmid,
    "StringList": XStringList,
    "DecimalStringList": XStringList,
    "PrecisionDateTime": XPrecisionDateTime,
    "PrecisionDateTimeFixed": XPrecisionDateTimeFixed,
    "PrecisionDateTimeRange": XPrecisionDateTimeRange,
    "Accession": XAccession,
    "Quality": XString,
    "DnaShape": XString,
    "Morphology": XString,
    "BaltimoreClass": XString
};

let errParseElements = "parseRecords: expected a sequence of 'record' elements. Actual name: "
let errParseAttributes = "parseRecords: expected a sequence of 'attribute' elements within a record: "

export function XMissing(name: string, el: Object): Attribute {
    return {
        type: AttributeType.Missing,
        name: name,
        data: {
            reason: el["reason"]
        }
    }
}

export function XString(name: string, el: Object): Attribute {
    return {
        type: AttributeType.String,
        name: name,
        data: {
            value: el as string
        } 
    }
}

export function XBoolean(name: string, el: Object): Attribute {
    let elData = el.toString()
    let dataVal: any
    if(elData === "true") {
        dataVal = true
    }
    else if(elData === "false") {
        dataVal = false
    }
    else {
        throw "XBoolean: boolean was neither 'true' nor 'false': " + elData;
    }
    return {
        type: AttributeType.Boolean,
        name: name,
        data: dataVal
    }
}

export function XPrecisionDecimalString(name: string, el: Object): Attribute {
    if(el["type"] === "PrecisionDecimalStringRange")
        return XPrecisionDecimalStringRange(name, el)
    else
        return XPrecisionDecimalStringFixed(name, el)
}

export function XPrecisionDecimalStringFixed(name: string, el: Object): Attribute {
    return {
        type: AttributeType.PrecisionDecimalStringFixed,
        name: name,
        data: {
            value: el["value"] as string,
        }
    }
}

export function XPrecisionDecimalStringRange(name: string, el: Object): Attribute {
    return {
        type: AttributeType.PrecisionDecimalStringRange,
        name: name,
        data: {
            min: el["max"] as string,
            max: el["min"] as string,
        }
    }
}

export function XDuration(name: string, el: Object): Attribute {
    return {
        type: AttributeType.Duration,
        name: name,
        data: {
            amount: XPrecisionDecimalString("amount", el["amount"]).data as String | Model.PrecisionDecimalStringRange,
            unit: el["unit"],
        }
    }
}

export function XRrnas(name: string, el: Object): Attribute{
    return {
        type: AttributeType.Rrnas,
        name: name,
        data: {
            rrna5S: Number(el["rrna5S"]),
            rrna16S: Number(el["rrna16S"]),
            rrna23S: Number(el["rrna23S"])
        }
    }
}

export function XOtherTyping(name: string, el: Object): Attribute{
    return {
        type: AttributeType.OtherTyping,
        name: name,
        data: {
            typing: el["typing"],
            term: el["term"],
        }
    }
}

export function XPosition(name: string, el: Object): Attribute{
    return {
        type: AttributeType.Position,
        name: name,
        data: {
            latitude: Number(el["latitude"]),
            longitude: Number(el["longitude"]),
        }
    }
}

export function XPrecisionDateTime(name: string, el: Object): Attribute {
    if(el["type"] === "PrecisionDateTimeRange")
        return XPrecisionDateTimeRange(name, el)
    else
        return XPrecisionDateTimeFixed(name, el)
}

export function XPrecisionDateTimeFixed(name: string, el: Object): Attribute {
    let dateObj: Date;
    let timePresent = false;
    let datePresent = false;
    let monthPresent = false;
    if(el.hasOwnProperty("year")) {
        dateObj = new Date(Date.parse(el["year"]))
    }
    else if(el.hasOwnProperty("yearMonth")) {
        dateObj = new Date(Date.parse(el["yearMonth"]))
        monthPresent = true
    }
    else if(el.hasOwnProperty("date")) {
        dateObj = new Date(Date.parse(el["date"]))
        datePresent = true
    }
    else if(el.hasOwnProperty("dateTime")) {
        dateObj = new Date(Date.parse(el["dateTime"]))
        timePresent = true
    }
    return {
        type: AttributeType.PrecisionDateTimeFixed,
        name: name,
        data: {
            year: dateObj.getUTCFullYear(),
            month: datePresent || monthPresent ? dateObj.getUTCMonth() + 1 : null,
            date: datePresent ? dateObj.getUTCDate() : null,
            hours: timePresent ? dateObj.getUTCHours() : null,
            minutes: timePresent ? dateObj.getUTCMinutes() : null,
            seconds: timePresent ? dateObj.getUTCSeconds() : null,
            milliseconds: timePresent ? dateObj.getUTCMilliseconds(): null,
        }
    }
}

export function XPrecisionDateTimeRange(name: string, el: Object): Attribute {
    return {
        type: AttributeType.PrecisionDateTimeRange,
        name: name,
        data: {
            from: XPrecisionDateTimeFixed("from", el["from"]).data as Model.PrecisionDateTimeFixed,
            to: XPrecisionDateTimeFixed("to", el["to"]).data as Model.PrecisionDateTimeFixed,
        }
    }
}

export function XStringList(name: string, el: Object): Attribute {
    let strList: Array<string> = []
    if(el["item"] instanceof Array) {
        for (let i=0; i<el["item"].length; i++){
            strList.push((el["item"])[i])
        }
    }
    else {
        strList.push(el["item"])
    }
    return {
        type: AttributeType.StringList,
        name: name,
        data: {
            items: strList
        }
    }
}

export function XAccession(name: string, el: Object): Attribute {
    let val: string
    let urlVal: string
    if(el.hasOwnProperty("#text")) {
        val = el["#text"]
    }
    else if(el.hasOwnProperty("value")) {
        val = el["value"]
    }
    else {
        val = el.toString()
    }
    if(el.hasOwnProperty("url")) {
        urlVal = el["url"]
    }
    else {
        urlVal = null
    }
    return {
        type: AttributeType.Accession,
        name: name,
        data: {
            value: val,
            url: urlVal
        }
    }
}

export function XAccessionList(name: string, el: Object): Attribute {
    let accList: Array<Model.Accession> = []
    if(el["accession"] instanceof Array) {
        for (let i=0; i<el["accession"].length; i++) {
            accList.push((XAccession("accession", (el["accession"])[i]).data) as Model.Accession)
        }
    }
    else {
        accList.push((XAccession("accession", el["accession"]).data) as Model.Accession)
    }
    return {
        type: AttributeType.AccessionList,
        name: name,
        data: {
            items: accList
        }
    }
}

export function XPublicationPmid(name: string, el: Object): Attribute {
    let pmiList: Array<Model.Accession | string> = []
    if(el.hasOwnProperty("url")) {
        pmiList.push(el["url"])
    }
    else if(el.hasOwnProperty("accession")) {
        if(el["accession"] instanceof Array) {
            for (let i=0; i<el["accession"].length; i++) {
                pmiList.push(XAccession("accession", (el["accession"])[i]).data as Model.Accession)
            }
        }
        else if (typeof el["accession"] === 'object') {
            pmiList.push((XAccession("accession", el["accession"]).data) as Model.Accession)
        }
        else {
            pmiList.push(el["accession"])
        }
    }
    return {
        type: AttributeType.PublicationPmid,
        name: name,
        data: {
            items: pmiList
        }
    }
}

export interface ParsedXSD {
    elementTypes: {[key:string]:string}
    elementsOrder: Array<string>
}

function getRecordsByTagName(schema: Xml2O.Node, name: string) {
    let ret: Array<Xml2O.Node> = []
    let e: Xml2O.Node
    for(let i=0; i<schema.length; i++) {
        e = schema[i]
        if(e.name === name) {
            ret.push(e);
        }
        if(e.length != 0) {
            ret = ret.concat(getRecordsByTagName(e, name));
        }
    }
    return ret;
}

export function parseXSD(schema: Xml2O.Node): ParsedXSD {
    function typeByName(schema: Xml2O.Node, name: string): Xml2O.Node {
        let types = getRecordsByTagName(schema, "xs:complexType")
        for(let i=0; i<types.length; i++) {
            if(types[i].getAttribute("name") === name) {
                return types[i];
            }
        }
    }
    let elementTypes: {[key:string]:string} = {};
    let elements = getRecordsByTagName(typeByName(schema, "MarRecord"), "xs:element");
    let elementsOrder: Array<string> = [];
    let elementName: string

    for(let i = 0; i < elements.length; i++) {
        if(!/Missing$/.test(elements[i].name)) {
            elementsOrder.push(elements[i].name);
        }
        elementTypes[elements[i].getAttribute("name")] = elements[i].getAttribute("type")
    }

    return {
        elementTypes: elementTypes,
        elementsOrder: elementsOrder
    };
}

export function ParseXmlDatabase(schema: Xml2O.Node, database: Object): MarDatabase {
    let parsedXSD = parseXSD(schema);
    let marRecords: Array<MarRecord> = []
    let recordsById: {[key:string]:MarRecord} = {}
    let records = database["record"] as Array<Object>;

    //console.log(records)

    let attributes: Array<Attribute>
    let mmpID: string
    let elementsKeys: Array<string>
    let elementsValues: Array<Object>
    let attributesByName: {[key:string]:Attribute}

    let parser: (attributeName: string, e: Object) => Attribute
    let attr: Attribute

    let marRecord: MarRecord

    for(let i=0; i < records.length; i++) {
        attributes = []
        elementsKeys = Object.keys(records[i]);
        elementsValues = Object.values(records[i]);
        mmpID = null
        attributesByName = {}

        for(let j=0; j < elementsKeys.length; j++) {
            if(elementsKeys[j] === "mmpID" || elementsKeys[j] === "cdbId") {
                mmpID = (elementsValues[j])["#text"] as string;
                delete (elementsValues[j])["url"]
            }
            parser = attributeTypeMappings[parsedXSD.elementTypes[elementsKeys[j]]];
            if(parser) {
                attr = parser(elementsKeys[j].replace(/Missing$/, ""), elementsValues[j]);
                attributes.push(attr);
                attributesByName[attr.name] = attr;
            }
        }
        marRecord = {
            attributes: attributes,
            mmpID: mmpID,
            attributesByName: attributesByName
        }
        marRecords.push(marRecord);
        recordsById[marRecord.mmpID] = marRecord
    }
    console.log("database type: ", database.databaseType, ", number of records: ", marRecords.length);
    return {
        type: database.databaseType,
        records: marRecords,
        defaultAttributesOrder: parsedXSD.elementsOrder,
        recordsById: recordsById
    }
}

export function ParseJsonDatabase(schema: Xml2O.Node, database: Object): MarDatabase {

    let parsedXSD = parseXSD(schema);
    let marRecords: Array<MarRecord> = []
    let recordsById: {[key:string]:MarRecord} = {}
    let records = database["records"]["record"] as Array<Object>;

    //console.log(records)

    let attributes: Array<Attribute>
    let mmpID: string
    let elementsKeys: Array<string>
    let elementsValues: Array<Object>
    let attributesByName: {[key:string]:Attribute}

    let parser: (attributeName: string, e: Object) => Attribute
    let attr: Attribute

    let marRecord: MarRecord
    let attributeType: string

    for(let i=0; i < records.length; i++) {
        attributes = []
        elementsKeys = Object.keys(records[i]);
        elementsValues = Object.values(records[i]);
        mmpID = null
        attributesByName = {}

        for(let j=0; j < elementsKeys.length; j++) {
            if(elementsKeys[j] === "mmpID" || elementsKeys[j] === "cdbId") {
                mmpID = (elementsValues[j])["value"] as string;
                delete (elementsValues[j])["url"]
            }
            attributeType = parsedXSD.elementTypes[elementsKeys[j]]
            parser = attributeTypeMappings[attributeType];
            if(parser) {
                attr = parser(elementsKeys[j].replace(/Missing$/, ""), elementsValues[j]);
                attributes.push(attr);
                attributesByName[attr.name] = attr;
            }
        }
        marRecord = {
            attributes: attributes,
            mmpID: mmpID,
            attributesByName: attributesByName
        }
        marRecords.push(marRecord);
        recordsById[marRecord.mmpID] = marRecord
    }
    console.log("database type: ", database["records"].databaseType, ", number of records: ", marRecords.length);
    return {
        type: database["records"].databaseType,
        records: marRecords,
        defaultAttributesOrder: parsedXSD.elementsOrder,
        recordsById: recordsById
    }
}
