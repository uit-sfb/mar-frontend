import { AttributeType, MarRecord, MarDatabase, Attribute, Missing, String } from "../model/Model";
import * as Model from "../model/Model";
import * as $ from "jquery"

let attributeTypeMappings: {[key:string]:(attributeName: string, e: Element) => Attribute} = {
    "xs:string": XString,
    "xs:date": XString,
    "xs:integer": XString,
    "xs:positiveInteger": XString,
    "xs:nonNegativeInteger": XString,
    "xs:boolean": XBoolean,
    "Missing": XMissing,
    "EnvPackage": XString,
    "InvestigationType": XString,
    "Rrnas": XRrnas,
    "GramStain": XString,
    "CellShape": XString,
    "TemperatureRange": XString,
    "Halotolerance": XString,
    "OxygenRequirement": XString,
    "OtherTyping": XOtherTyping,
    "GenomeStatus": XString,
    "HostSex": XString,
    "PrecisionDecimalString": XPrecisionDecimalString,
    "PrecisionDecimalStringRange": XPrecisionDecimalStringRange,
    "PrecisionDecimalStringFixed": XPrecisionDecimalStringFixed,
    "Duration": XDuration,
    "DurationUnit": XString,
    "Position": XPosition,
    "DecimalString": XString,
    "AccessionList": XAccessionList,
    "PublicationPmid": XPublicationPmid,
    "StringList": XStringList,
    "DecimalStringList": XStringList,
    "PrecisionDateTime": XPrecisionDateTime,
    "PrecisionDateTimeFixed": XPrecisionDateTimeFixed,
    "PrecisionDateTimeRange": XPrecisionDateTimeRange,
    "Accession": XAccession
};

function getElementsByTagName(element: Element, name: string): Array<Element> {
    let elements = element.childNodes
    let ret: Array<Element> = []
    for(let i=0; i<elements.length; i++) {
        let e = elements[i] as Element
        if(e.tagName === name) {
            ret.push(e);
        }
        if(e.nodeType === Node.ELEMENT_NODE) {
            ret = ret.concat(getElementsByTagName(e, name));
        }
    }
    return ret;
}

export function XMissing(name: string, el: Element): Attribute {
    let attr: Missing = {
        reason: el.getElementsByTagName("reason")[0].textContent
    }

    return {
        type: AttributeType.Missing,
        name: name,
        data: attr
    }
}

export function XString(name: string, el: Element): Attribute {
    let text = "";
    if(el.firstChild != null) {
        text = el.firstChild.textContent;
    }
    let data: String = {
        value: text
    }
    return {
        type: AttributeType.String,
        name: name,
        data: data 
    }
}

export function XBoolean(name: string, el: Element): Attribute {
    let value: boolean
    let raw = el.firstChild.textContent;
    switch(raw) {
        case "true":
            value = true;
            break;
        case "false":
            value = false;
            break;
        default:
            throw "XBoolean: boolean was neither 'true' nor 'false': " + raw; 
    }


    return {
        type: AttributeType.Boolean,
        name: name,
        data: value
    }
}

export function XPrecisionDecimalString(name: string, el: Element): Attribute {
    let type = el.attributes.getNamedItem("xsi:type").textContent;
    if(type === "PrecisionDecimalStringFixed")
        return XPrecisionDecimalStringFixed(name, el)
    else
        return XPrecisionDecimalStringRange(name, el)
}

export function XPrecisionDecimalStringFixed(name: string, el: Element): Attribute {
    let data: String = {
        value: getElementsByTagName(el, "value")[0].textContent,
    }
    return {
        type: AttributeType.PrecisionDecimalStringFixed,
        name: name,
        data: data
    }
}

export function XPrecisionDecimalStringRange(name: string, el: Element): Attribute {
    let data: Model.PrecisionDecimalStringRange = {
        min: getElementsByTagName(el, "min")[0].textContent,
        max: getElementsByTagName(el, "max")[0].textContent,
    }
    return {
        type: AttributeType.PrecisionDecimalStringRange,
        name: name,
        data: data
    }
}

export function XDuration(name: string, el: Element): Attribute {
    let data: Model.Duration = {
        amount: XPrecisionDecimalString("amount", getElementsByTagName(el, "amount")[0]).data as String | Model.PrecisionDecimalStringRange,
        unit: getElementsByTagName(el, "unit")[0].textContent,
    }
    return {
        type: AttributeType.Duration,
        name: name,
        data: data
    }
}

export function XRrnas(name: string, el: Element): Attribute{
    let value: Model.Rrnas = {
        rrna5S: Number(el.getElementsByTagName("rrna5S")[0].textContent),
        rrna16S: Number(el.getElementsByTagName("rrna16S")[0].textContent),
        rrna23S: Number(el.getElementsByTagName("rrna23S")[0].textContent)
    }
    return {
        type: AttributeType.Rrnas,
        name: name,
        data: value
    }
    
}

export function XOtherTyping(name: string, el: Element): Attribute{
    let value: Model.OtherTyping = {
        typing: el.getElementsByTagName("typing")[0].textContent,
        term: el.getElementsByTagName("term")[0].textContent,
    }
    return {
        type: AttributeType.OtherTyping,
        name: name,
        data: value
    }
    
}

export function XPosition(name: string, el: Element): Attribute{
    let value: Model.Position = {
        latitude: Number(el.getElementsByTagName("latitude")[0].textContent),
        longitude: Number(el.getElementsByTagName("longitude")[0].textContent),
    }
    return {
        type: AttributeType.Position,
        name: name,
        data: value
    }
    
}

export function XPrecisionDateTime(name: string, el: Element): Attribute {
    let type = el.attributes.getNamedItem("xsi:type").textContent;
    if(type === "PrecisionDateTimeFixed")
        return XPrecisionDateTimeFixed(name, el)
    else
        return XPrecisionDateTimeRange(name, el)
}

export function XPrecisionDateTimeFixed(name: string, el: Element): Attribute {
    let subelem = "";
    let timePresent = false;
    if(getElementsByTagName(el, "year")[0] != null) {
        subelem = getElementsByTagName(el, "year")[0].textContent
    }
    else if(getElementsByTagName(el, "yearMonth")[0] != null) {
        subelem = getElementsByTagName(el, "yearMonth")[0].textContent
    }
    else if(getElementsByTagName(el, "date")[0] != null) {
        subelem = getElementsByTagName(el, "date")[0].textContent
    }
    else if(getElementsByTagName(el, "dateTime")[0] != null) {
        subelem = getElementsByTagName(el, "dateTime")[0].textContent
        timePresent = true
    }
    let dateObj = new Date(Date.parse(subelem))
    let data: Model.PrecisionDateTimeFixed = {
        year: dateObj.getUTCFullYear(),
        month: dateObj.getUTCMonth() + 1,
        date: dateObj.getUTCDate(),
        hours: timePresent ? dateObj.getUTCHours() : null,
        minutes: timePresent ? dateObj.getUTCMinutes() : null,
        seconds: timePresent ? dateObj.getUTCSeconds() : null,
        milliseconds: timePresent ? dateObj.getUTCMilliseconds(): null,
    }
    return {
        type: AttributeType.PrecisionDateTimeFixed,
        name: name,
        data: data
    }
}

export function XPrecisionDateTimeRange(name: string, el: Element): Attribute {
    let data: Model.PrecisionDateTimeRange = {
        from: XPrecisionDateTimeFixed("from", getElementsByTagName(el, "from")[0]).data as Model.PrecisionDateTimeFixed,
        to: XPrecisionDateTimeFixed("to", getElementsByTagName(el, "to")[0]).data as Model.PrecisionDateTimeFixed,
    }
    return {
        type: AttributeType.PrecisionDateTimeRange,
        name: name,
        data: data
    }
}

export function XStringList(name: string, el: Element): Attribute {
    let xlist = el.getElementsByTagName("item")
    let strList: Array<string> = []
    
    for (let i=0; i< xlist.length; i++){
        strList.push(xlist[i].textContent)
    }
    let value: Model.StringList = {
        items: strList
    }
    return {
        type: AttributeType.StringList,
        name: name,
        data: value
    }
}

export function XAccession(name: string, el: Element): Attribute {
    let child = el.firstChild;
    let url: string = null;
    let urlAttr = el.attributes.getNamedItem("url");
    if(urlAttr) {
        url = urlAttr.textContent;
    }

    let data: Model.Accession = {
        value: child.textContent,
        url: url
    }

    return {
        type: AttributeType.Accession,
        name: name,
        data: data
    }
}

export function XAccessionList(name: string, el: Element): Attribute {
    let xlist = el.getElementsByTagName("accession")
    let accList: Array<Model.Accession> = []
    
    for (let i=0; i< xlist.length; i++){
        let cur = (XAccession("accession", xlist[i]).data) as Model.Accession
        accList.push(cur)
    }
    
    let value: Model.AccessionList = {
        items: accList
    }
    return {
        type: AttributeType.AccessionList,
        name: name,
        data: value
    }
}

export function XPublicationPmid(name: string, el: Element): Attribute {
    let xlist = el.childNodes
    let pmiList: Array<Model.Accession | string> = []
    
    for (let i=0; i< xlist.length; i++){
        if (xlist[i].nodeType === Node.ELEMENT_NODE) {
            switch(xlist[i].nodeName){
                case "url":
                    pmiList.push(xlist[i].textContent)
                    break
                case "accession":
                    let acc = XAccession("accession", xlist[i] as Element).data as Model.Accession
                    pmiList.push(acc)
                    break
            }
        }
    }
    
    let value: Model.PublicationPmid = {
        items: pmiList
    }
    return {
        type: AttributeType.PublicationPmid,
        name: name,
        data: value
    }
}

export interface ParsedXSD {
    elementTypes: {[key:string]:string}
    elementsOrder: Array<string>
}

export function parseXSD(schema: XMLDocument): ParsedXSD {
    function typeByName(element: Element, name: string): Element {
        let types = getElementsByTagName(element, "xs:complexType")
        for(let i=0; i < types.length; i++) {
            let cur = types[i];
            if(cur.attributes.getNamedItem("name").value === name) {
                return cur;
            }
        }
    }

    let marRecord = typeByName(schema.documentElement, "MarRecord");
    
    let elementTypes: {[key:string]:string} = {};

    let elements = getElementsByTagName(marRecord, "xs:element");
    let elementsOrder: Array<string> = [];

    for(let i = 0; i < elements.length; i++) {
        let el = elements[i]
        let elementName = el.attributes.getNamedItem("name").value;
        if(!/Missing$/.test(elementName)) {
            elementsOrder.push(elementName);
        }
        elementTypes[elementName] = el.attributes.getNamedItem("type").value 
    }

    return {
        elementTypes: elementTypes,
        elementsOrder: elementsOrder
    };
}

export function ParseDatabase(schema: XMLDocument, database: XMLDocument): MarDatabase {
    let parsedXSD = parseXSD(schema);
    let marRecords: Array<MarRecord> = []
    
    let records = database.documentElement.childNodes;
    for(let i=0; i < records.length; i++) {
        let record = records[i]
        if(record.nodeType === Node.TEXT_NODE || record.nodeType === Node.COMMENT_NODE) {
            continue;
        }
        if((record.nodeType !== Node.ELEMENT_NODE) || (record.nodeName !== "record")) {
            let err = "parseRecords: expected a sequence of 'record' elements. Actual name: '" + record.nodeName + "'."
            console.log(record)
            console.error(err)
            throw err
        }

        let attributes: Array<Attribute> = []
        let mmpID: string
        let elements = record.childNodes;
        for(let j=0; j < elements.length; j++) {
            let element = elements[j];
            if(element.nodeType === Node.TEXT_NODE || element.nodeType === Node.COMMENT_NODE) {
                continue;
            }
            if(element.nodeType !== Node.ELEMENT_NODE) {
                let err = "parseRecords: expected a sequence of attribute elements within a record."
                console.error(err)
                console.log(element)
                throw err
            }

            if(element.nodeName === "mmpID") {
                mmpID = element.textContent;
            }
            
            let type = parsedXSD.elementTypes[element.nodeName];
            let parser = attributeTypeMappings[type];
            if(parser) {
                let attr = parser(element.nodeName.replace(/Missing$/, ""), element as Element);
                attributes.push(attr);
            }
        }

        let attributesByName: {[key:string]:Attribute} = {}
        for(let attr of attributes) {
            attributesByName[attr.name] = attr;
        }

        let marRecord: MarRecord = {
            attributes: attributes,
            mmpID: mmpID,
            attributesByName: attributesByName
        }
        marRecords.push(marRecord);
    }

    let recordsById: {[key:string]:MarRecord} = {}
    for(let record of marRecords) {
        recordsById[record.mmpID] = record
    }

    let databaseType = database.documentElement.getAttribute("databaseType");
    console.log("database type: ", databaseType);
    
    return {
        type: databaseType,
        records: marRecords,
        defaultAttributesOrder: parsedXSD.elementsOrder,
        recordsById: recordsById
    }
}