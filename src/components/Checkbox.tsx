import * as React from "react";

export interface CheckboxProps {
    selected: boolean
    onSelect: (selected: boolean) => void
    ariaLabel?: string
}

export class Checkbox extends React.Component<CheckboxProps, {}> {
    constructor() {
        super()
        this.onChange = this.onChange.bind(this);
    }

    onChange(event: React.FormEvent<HTMLInputElement>) {
        this.props.onSelect(event.currentTarget.checked)
    }

    render() {
        return <input type="checkbox" aria-label={this.props.ariaLabel} onChange={this.onChange} checked={this.props.selected} />
    }
}