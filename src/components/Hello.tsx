import * as React from "react";

export interface HelloProps { }

export class Hello extends React.Component<HelloProps, {}> {
    render() {
        return <h1>Hello!</h1>;
    }
}