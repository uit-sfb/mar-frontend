import * as $ from "jquery";
import * as React from "react";
import * as Model from "../model/Model";
import { Panel, Table, Button, ButtonGroup, ButtonToolbar, Glyphicon, OverlayTrigger, Tooltip, Popover } from "react-bootstrap-typescript";

export interface SelectedRecordsOptionsProps { 
    records: Set<string>,
    fileType: {[key:string]:boolean}
    onSelectFileType: (fileType: string) => void
    onUnselectAll: () => void
    databaseType: string
}

export class SelectedRecordsOptions extends React.Component<SelectedRecordsOptionsProps, {}> {
    render() {
        let downloadTooltip = <Tooltip>Download selected records...</Tooltip>
        let unselectTooltip = <Tooltip>Unselect all selected records</Tooltip>

        let endpoint: string
        $.ajax({
            url: "data/endpoints.json"
        }).then(jsonRes => {
            endpoint = jsonRes.marDatabaseServer
        })

        let fileTypeButtons: Array<JSX.Element> = []
        for(let item in this.props.fileType) {
            fileTypeButtons.push(
                // 'disabled={item!="tsv"}' will be deleted when other file types are implemented
                <Button
                    active={this.props.fileType[item]}
                    disabled={item!="tsv"}
                    bsSize="small"
                    onClick={() => this.props.onSelectFileType(item)}>
                    {item}
                </Button>
            )
        }

        let onDownload = function() {
            let fileTypeString: string = ""
            for(let item in this.props.fileType) {
                if(this.props.fileType[item]) {
                    fileTypeString = item
                }
            }
            let recordIDsString: string = ""
            for(let item of this.props.records) {
                recordIDsString += item + "."
            }
            let urlQuery = "db=" + this.props.databaseType + "&format=" + fileTypeString + "&ids=" +
                recordIDsString.substring(0, recordIDsString.length - 1);
            let url: string = endpoint + "/index.html" + "?" + urlQuery;
            console.log(url)
            window.open(url, "_blank");
        }.bind(this);

        return (
            <div>
                <ButtonToolbar>
                    <tr>
                        <ButtonGroup>
                            <OverlayTrigger placement="top" overlay={unselectTooltip}>
                                <Button onClick={this.props.onUnselectAll} bsStyle="danger"><Glyphicon glyph="remove"/> Unselect</Button>
                            </OverlayTrigger>
                            <Button disabled={this.props.records.size==0} onClick={onDownload} bsStyle="info"><Glyphicon glyph="download"/> Download</Button>
                        </ButtonGroup>
                    </tr>
                    <p/>
                    <tr>
                        <ButtonGroup>
                            {fileTypeButtons}
                        </ButtonGroup>
                    </tr>
                </ButtonToolbar>
                <p/>
                <p style={{ "margin": "0"}}><b>Number of selected records: </b>{this.props.records.size}</p>
            </div>
        );
    }
}