import * as React from "react";

import { Panel, Table } from "react-bootstrap-typescript";

export interface ITablePanelProps {
    heading: string
    elements: Array<[string,string]>
    propertyHeading?: string
    valueHeading?: string
}

export class TablePanel extends React.Component<ITablePanelProps,{}> {
    render() {
        let tableContents: Array<JSX.Element> = []

        for(let item of this.props.elements) {
            tableContents.push(
                <tr>
                    <td className="col-md-3">{item[0]}</td>
                    <td>{item[1]}</td>
                </tr>
            )
        }

        return (
            <Panel header={this.props.heading}>
                <Table condensed hover>
                    <thead>
                        <tr>
                            <th>{this.props.propertyHeading || ""}</th>
                            <th>{this.props.valueHeading || ""}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableContents}
                    </tbody>
                </Table>
            </Panel>
        )
    }
}