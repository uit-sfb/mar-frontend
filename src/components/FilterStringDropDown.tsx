import * as React from "react";
import * as Antd from "antd";
import * as Model from "../model/Model"
import * as Orderings from "../utils/Orderings";

function findMostCommonValues(values: Array<string>): Array<[string, number]> {
    let count: {[key:string]:number} = {}

    for(let value of values) {
        let num = count[value] || 0;
        count[value] = num + 1;
    }

    let res: Array<[string, number]> = []
    for(let key in count) {
        if((key !== null) && typeof key !== 'undefined') {
            res.push([key, count[key]]);
        }
    }

    res.sort((a,b) => b[1] - a[1]);
    return res;
}

let STRING_LIST_ITEM_INNER_SEPARATOR = " - "

export interface FilterStringDropDownProps {
    records: Array<Model.MarRecord>
    attributeName: string
    onChange: (value:string) => void
    label?: string
    numValuesToDisplay?: number
 }

export class FilterStringDropDown extends React.Component<FilterStringDropDownProps, {}> {

    render() {

        let mostCommon = findMostCommonValues(this.props.records.map(r => {
            return r.attributesByName[this.props.attributeName];
        }).filter(a => a && (a.type === Model.AttributeType.String || a.type === Model.AttributeType.Accession)).map(attr => {
            let data = attr.data as Model.String | Model.Accession
            return data.value
        }))//.slice(0,this.props.numValuesToDisplay || 20);

        let options: Array<JSX.Element> = mostCommon.map(v => <Antd.Select.Option value={v[0]} key={v[0]}>{v[0]} ({v[1]})</Antd.Select.Option>)

        let label = this.props.label || Orderings.AttributeLabel(this.props.attributeName);

        return (
              <Antd.Select
                showSearch
                style={{"width": "100%"}}
                placeholder={"Select " + label}
                optionFilterProp="children"
                onChange={this.props.onChange}
                disabled={mostCommon.length === 0}>
                {options}
            </Antd.Select>
        );
    }
}

export class FilterStringListDropDown extends React.Component<FilterStringDropDownProps, {}> {

    render() {
    
        let values = this.props.records.map(r => {
            return r.attributesByName[this.props.attributeName];
        }).filter(a => a && (a.type === Model.AttributeType.StringList || a.type === Model.AttributeType.AccessionList)).map(attr => {
            let data = attr.data as Model.StringList | Model.AccessionList
            return data.items
        })
        
        values = [].concat.apply([], values);
        for (let v of values)
        {
            if(v.search(STRING_LIST_ITEM_INNER_SEPARATOR) > 0)
            {
                let split = v.split(STRING_LIST_ITEM_INNER_SEPARATOR)
                values[values.indexOf(v)] = split[0]
                for (var i = 1; i < split.length; i++) {
                    values.push(split[i]);
                }
            }
        }
        values = findMostCommonValues(values);

        let options: Array<JSX.Element>
        if(this.props.attributeName === "antismashTypes")
        {
            values = values.sort()
            options = values.map(v => <Antd.Select.Option value={v[0]} key={v[0]}>{v[0]}</Antd.Select.Option>)
        }
        else
        {
            options = values.map(v => <Antd.Select.Option value={v[0]} key={v[0]}>{v[0]} ({v[1]})</Antd.Select.Option>)
        }

        let label = this.props.label || Orderings.AttributeLabel(this.props.attributeName)

        return (
            <Antd.Select
                showSearch
                style={{"width": "100%"}}
                placeholder={"Select " + label}
                optionFilterProp="children"
                onChange={this.props.onChange}
                disabled={values.length === 0}>
                {options}
            </Antd.Select>
        );
    }
}