import * as React from "react";
import * as Model from "../model/Model";
import * as Router from "react-router";
import { Hello } from "../components/Hello"
import { Navbar, Nav, NavItem } from "react-bootstrap-typescript"

export class AppFrameCovid extends React.Component<{}, {}> {
    // <DatabaseOverviewPage database={this.props.database}/>
    
    render() {
        return (
            <div className="container-fluid">
                <Nav bsStyle="pills">
                    <NavItem href="#/records">Browse</NavItem>
                </Nav>
                {this.props.children}
            </div>
        );
    }
}