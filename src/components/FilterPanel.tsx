import * as React from "react";
import * as Model from "../model/Model";
import * as Filters from "../utils/Filters";
import * as $ from "jquery"
import * as FR from "./FilterRange"; 
import { FilterTextSearch } from "./FilterTextSearch";
import { FilterStringDropDown,  FilterStringListDropDown} from "../components/FilterStringDropDown";
import { FilterRange } from "./FilterRange";
import * as Orderings from "../utils/Orderings";

export interface FilterPanelProps {
    database: Model.MarDatabase
    records: Array<Model.MarRecord>
    filters: Filters
    numFilteredRecords: number
    onFiltersChange: (filters: Filters) => void
}

export interface Filters {
    envPackage: Filters.StringFilterParams | null
    envBiome: Filters.StringFilterParams | null
    envFeature: Filters.StringFilterParams | null
    envMaterial: Filters.StringFilterParams | null
    isolationCountry: Filters.StringFilterParams | null
    collectionDate: Filters.RangeFilterParams
    depth: Filters.RangeFilterParams
    phylum: Filters.StringFilterParams | null
    family: Filters.StringFilterParams | null
    genus: Filters.StringFilterParams | null
    analysisProjectType: Filters.StringFilterParams | null
    hostScientificName: Filters.StringFilterParams | null
    oxygenRequirement: Filters.StringFilterParams | null
    temperatureRange: Filters.StringFilterParams | null
    halotolerance: Filters.StringFilterParams | null
    projectName: Filters.StringFilterParams | null
    textSearch: Filters.TextFilterParams | null
    antismashTypes: Filters.StringFilterStringList | null
    chebiName: Filters.StringFilterStringList | null
    // todo cultureCollection
    //Covid filters
    hostScientificName: Filters.StringFilterParams | null
    //hostAge: Filters.RangeFilterParams | null
    hostSex: Filters.StringFilterParams | null
    bodySampleSite: Filters.StringFilterParams | null
    sampleType: Filters.StringFilterParams | null
    isolationSource: Filters.StringFilterParams | null

    [name: string]: Filters.StringFilterParams | Filters.StringFilterStringListParams | Filters.RangeFilterParams | Filters.TextFilterParams | null
}

export class FilterPanel extends React.Component<FilterPanelProps, {}> {
    constructor() {
        super()

        this.copyFilters = this.copyFilters.bind(this);
    }

    copyFilters() {
        let newFilters: Filters = {} as Filters
        $.extend(newFilters, this.props.filters)
        return newFilters;
    }

    render() {
        let self = this;
        let records = this.props.records;
        let filters = this.props.filters;
        
        let usedFilters: Array<JSX.Element> = [];
        let availableFilters: Array<JSX.Element> = [];

        function hasAttr(attrName: string): boolean {
            for(let r of self.props.database.records) {
                if(r.attributesByName[attrName]) {
                    return true;
                }
            }
            return false;
        }

        function stringFilter(attrName: string, description: string) {
            if(!hasAttr(attrName)) {
                return;
            }
            let onSelect = ((value: string) => {
                let newFilters: Filters = self.copyFilters();
                newFilters[attrName] = {
                    equals: value
                };
                self.props.onFiltersChange(newFilters);
            });

            let onRemove = (() => {
                let newFilters: Filters = self.copyFilters();
                newFilters[attrName] = null;
                self.props.onFiltersChange(newFilters);
            });

            if(filters[attrName] === null) {
                availableFilters.push(
                    <div>
                        <h5>{description}</h5>
                        <FilterStringDropDown records={records} attributeName={attrName} onChange={onSelect} key={attrName} />
                    </div>
                );
            } else {
                let filterParams = filters[attrName] as Filters.StringFilterParams
                usedFilters.push(
                    <p key={attrName}>
                        {description}: <i>{filterParams.equals}</i>&nbsp;<a onClick={onRemove}>[x]</a>
                    </p>
                );
            }
        }
        
        function stringFilterStringList(attrName: string, description: string)
        {
            if(!hasAttr(attrName)) {
                return;
            }
            let onSelect = ((value: string) => {
                let newFilters: Filters = self.copyFilters();
                newFilters[attrName] = {
                    equals: value
                };
                self.props.onFiltersChange(newFilters);
            });

            let onRemove = (() => {
                let newFilters: Filters = self.copyFilters();
                newFilters[attrName] = null;
                self.props.onFiltersChange(newFilters);
            });

            if(filters[attrName] === null) {
                availableFilters.push(
                    <div>
                        <h5>{description}</h5>
                        <FilterStringListDropDown records={records} attributeName={attrName} onChange={onSelect} key={attrName} />
                    </div>
                );
            } else {
                let filterParams = filters[attrName] as Filters.StringFilterStringListParams
                usedFilters.push(
                    <p key={attrName}>
                        {description}: <i>{filterParams.equals}</i>&nbsp;<a onClick={onRemove}>[x]</a>
                    </p>
                );
            }
        }

        function rangeFilter(attrName: string, description: string, mapper: Filters.AttributeRangeMapper, minDescr: string, maxDescr: string) {
            if(!hasAttr(attrName)) {
                return;
            }
            let onSelect = ((newValue: Filters.RangeFilterParams) => {
                let newFilters: Filters = self.copyFilters();
                newFilters[attrName] = {
                    min: newValue.min,
                    max: newValue.max
                }
                self.props.onFiltersChange(newFilters);
            });
            let params = filters[attrName] as Filters.RangeFilterParams;
            availableFilters.push(
                <div>
                    <h5>{description}</h5>
                    <FilterRange
                        params={params}
                        attributeName={attrName}
                        records={self.props.database.records}
                        onChange={onSelect}
                        mapper={mapper}/>
                </div>
            );

            let onRemoveMin = (() => {
                let newFilters: Filters = self.copyFilters();
                newFilters[attrName] = {
                    min: -Infinity,
                    max: params.max
                };
                self.props.onFiltersChange(newFilters);
            });

            let onRemoveMax = (() => {
                let newFilters: Filters = self.copyFilters();
                newFilters[attrName] = {
                    min: params.min,
                    max: Infinity
                };
                self.props.onFiltersChange(newFilters);
            });

            if(filters[attrName] !== null) {
                if(params.min != -Infinity) {
                    usedFilters.push(<p key={attrName.concat("_min")}>{description} <i title="inclusive">{minDescr} {params.min}</i> <a onClick={onRemoveMin}>[x]</a></p>);
                }

                if(params.max != Infinity) {
                    usedFilters.push(<p key={attrName.concat("_max")}>{description} <i title="inclusive">{maxDescr} {params.max}</i> <a onClick={onRemoveMax}>[x]</a></p>);
                }
            }
        }

        function sf(attrName: string) {
            stringFilter(attrName, Orderings.AttributeLabel(attrName));
        }
        
        function sfStringList(attrName: string) {
            stringFilterStringList(attrName, Orderings.AttributeLabel(attrName));
        }        

        //sf("projectName")
        if(self.props.database.type == "sarscov2db")
        {
            //rangeFilter("hostAge", "Host Age", Filters.DecimalUnitRangeMapper("y"), "min", "max")
            sf("hostSex")
            sf("bodySampleSite")
            sf("sampleType")
            sf("isolationSource")
        }
        else
        {
            sf("family")
            sf("genus")
            sf("envPackage")
        }
        sf("envBiome")
        sf("envFeature")
        sf("envMaterial")
        sf("analysisProjectType")
        sf("phylum")
        sf("isolationCountry")
        sf("temperatureRange")
        sf("halotolerance")
        sf("oxygenRequirement")
        sf("hostScientificName")

        rangeFilter("collectionDate", "Collection Date", Filters.YearRangeMapper, "after", "before");
        rangeFilter("depth", "Depth", Filters.DecimalUnitRangeMapper("m"), "below", "above")
        
        sfStringList("antismashTypes")
        sfStringList("chebiName")

        let resetAll = <a onClick={clear}>Reset all filters</a>
        function clear() {
            self.props.onFiltersChange({} as Filters);
        }

        return (
            <div>
                {usedFilters}
                {resetAll}
                <p/>
                <p style={{ "margin": "0"}}><b>Number of filtered records: </b>{self.props.numFilteredRecords}</p>
                <hr/>
                {availableFilters}
            </div>
        );
    }
}