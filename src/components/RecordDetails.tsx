import * as React from "react";
import * as Model from "../model/Model";
import * as Orderings from "../utils/Orderings";
import { RenderMedium } from "../utils/AttributeRenderer";
import { AttributesDropDown } from "./AttributesDropDown";
import * as FormatText from "../utils/FormatText";
import { PageHeader } from "react-bootstrap-typescript";

export interface RecordDetailsProps { 
    record: Model.MarRecord
    databaseType: string
    recordId: string
}

export interface RecordDetailsState {
    collapsed: Array<boolean>
}

// todo: hard-code attributes/categories, put them into schema later.

export class RecordDetails extends React.Component<RecordDetailsProps, RecordDetailsState> {
    constructor() {
        super()
        let cats = Orderings.Categories();
        let collapsed: Array<boolean> = Array(cats.length);
        for(let i=0; i < cats.length; i++) {
            collapsed[i] = !cats[i].expanded
        }
        this.state = {
            collapsed: collapsed
        }
        this.onToggleCollapse = this.onToggleCollapse.bind(this);
        this.onExpandAll = this.onExpandAll.bind(this);
        this.onCollapseAll = this.onCollapseAll.bind(this);
    }

    onToggleCollapse(idx: number) {
        let collapsed = this.state.collapsed.slice();
        collapsed[idx] = !collapsed[idx];

        this.setState({
            collapsed: collapsed
        })
    }

    onExpandAll() {
        let collapsed = this.state.collapsed.slice();
        for(let i=0; i < collapsed.length; i++) {
            collapsed[i] = false;
        }
        this.setState({
            collapsed: collapsed
        })
    }

    onCollapseAll() {
        let collapsed = this.state.collapsed.slice();
        for(let i=0; i < collapsed.length; i++) {
            collapsed[i] = true;
        }
        this.setState({
            collapsed: collapsed
        })
    }

    render() {
        let record = this.props.record;
        let categories = Orderings.Categories()
        let headerName = ""
        //console.log(record)

        let dropDowns: Array<JSX.Element> = []

        for(let i=0; i<categories.length; i++) {
            let category = categories[i]

            if(category.databaseType && category.databaseType.indexOf(this.props.databaseType) == -1) {
                continue;
            }

            let elements: Array<[string, JSX.Element]> = [];

            // The "this.props.databaseType.indexOf("marref") != -1" condition will be modified/deleted when antiSMASH for MarDB/MarCat is ready.
            if((this.props.databaseType.indexOf("marref") != -1 || this.props.databaseType.indexOf("mardb") != -1) &&
                category.heading == "Secondary metabolities") {
                let antiSmashLink = "https://public.sfb.uit.no/" + Orderings.DatabaseLabel(this.props.databaseType) + "/antiSMASH/" + record.mmpID + "/"
                let antiSmashAttr = {
                    type: Model.AttributeType.Accession,
                    name: "antiSMASH",
                    data: {
                        value: antiSmashLink,
                        url: antiSmashLink
                    }
                }
                elements.push([Orderings.AttributeLabel(antiSmashAttr.name), RenderMedium(antiSmashAttr, name)])
            }

            for(let attrName of category.attributeNames) {
                //console.log(attrName, this.props.recordId);
                let attr = record.attributesByName[attrName];
                if(attr != null) {
                    if((attrName == "fullScientificName" && (this.props.databaseType.indexOf("marref") != -1 || this.props.databaseType.indexOf("mardb") != -1)) ||
                        (attrName == "projectName" && category.databaseType == "marcat")) {
                        headerName = (attr.data as Model.String).value
                    }
                    elements.push([Orderings.AttributeLabel(attrName), RenderMedium(attr, name)])
                } else {
                    console.error("Unrecognized attribute: ", attrName);
                }
            }
            if(elements.length !== 0) {
                dropDowns.push(
                    <AttributesDropDown
                        bsStyle={category.bsStyle}
                        heading={category.heading}
                        elements={elements}
                        collapsed={this.state.collapsed[i]}
                        onToggle={() => this.onToggleCollapse(i)} />
                );
            }
        }

        return (
            <div>
                <PageHeader>{headerName}</PageHeader>
                <p>
                    <a onClick={this.onExpandAll}><span className={"glyphicon glyphicon-plus"}></span> Expand all</a>
                    &nbsp;&nbsp;/&nbsp;&nbsp;
                    <a onClick={this.onCollapseAll}><span className={"glyphicon glyphicon-minus"}></span> Collapse all</a>
                    &nbsp;
                    <a href={"https://public.sfb.uit.no/" + Orderings.DatabaseLabel(this.props.databaseType) + "/Genomes/" + this.props.recordId} target="_blank" style={{float: 'right'}}>Download Sequences</a>
                </p>
                {dropDowns}
            </div>
        )
    }
}