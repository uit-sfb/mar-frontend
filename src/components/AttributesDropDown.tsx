import * as React from "react";

import { Panel, Table, Collapse } from "react-bootstrap-typescript";

export interface IAttributesDropDownProps {
    heading: string
    elements: Array<[string,string | JSX.Element | Array<JSX.Element>]>
    collapsed: boolean
    onToggle: () => void
    bsStyle?: string
    propertyHeading?: string
    valueHeading?: string
}

export class AttributesDropDown extends React.Component<IAttributesDropDownProps,{}> {
    render() {
        let tableContents: Array<JSX.Element> = []

        for(let item of this.props.elements) {
            tableContents.push(
                <tr>
                    <td className="col-md-3">{item[0]}</td>
                    <td>{item[1]}</td>
                </tr>
            )
        }

        let plusMinusClassName: string
        if(this.props.collapsed) {
            plusMinusClassName = "glyphicon-plus";
        } else {
            plusMinusClassName = "glyphicon-minus";
        }

        let plusMinus = <span className={"glyphicon " + plusMinusClassName}></span>;

        let heading = <div onClick={this.props.onToggle}>{plusMinus}&nbsp;{this.props.heading}</div>
        let body: Array<JSX.Element> = []
        if(!this.props.collapsed) {
            body.push(
                <Table condensed hover>
                    <thead>
                        <tr>
                            <th>{this.props.propertyHeading || ""}</th>
                            <th>{this.props.valueHeading || ""}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableContents}
                    </tbody>
                </Table>
            );
        }
        return (
            <Panel header={heading} bsStyle={this.props.bsStyle}>
                {body}
            </Panel>
        )
    }
}