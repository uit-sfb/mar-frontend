import * as React from "react";

import { Panel, Table, Collapse } from "react-bootstrap-typescript";

export interface DropDownPanelProps {
    heading: string
    collapsed: boolean
    onToggle: () => void
    bsStyle?: string
    propertyHeading?: string
    valueHeading?: string
}

export class DropDownPanel extends React.Component<DropDownPanelProps,{}> {
    render() {
        let plusMinusClassName: string
        if(this.props.collapsed) {
            plusMinusClassName = "glyphicon-plus";
        } else {
            plusMinusClassName = "glyphicon-minus";
        }

        let plusMinus = <span className={"glyphicon " + plusMinusClassName}></span>;

        let heading = <div onClick={this.props.onToggle}>{plusMinus}&nbsp;{this.props.heading}</div>
        let body: React.ReactNode = []
        if(!this.props.collapsed) {
            body = this.props.children;
        }
        return (
            <Panel header={heading} bsStyle={this.props.bsStyle}>
                {body}
            </Panel>
        )
    }
}