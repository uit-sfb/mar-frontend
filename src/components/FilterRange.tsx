import * as React from "react";
import * as Model from "../model/Model"
import * as Antd from "antd";
import * as Filters from "../utils/Filters";

let T = Model.AttributeType

export interface FilterRangeProps {
    records: Array<Model.MarRecord>
    attributeName: string
    onChange: (newValue: Filters.RangeFilterParams) => void
    mapper: Filters.AttributeRangeMapper
    params: Filters.RangeFilterParams
    //label?: string
    //numValuesToDisplay?: number
}

export class FilterRange extends React.Component<FilterRangeProps, {}> {
    render() {
        
        let self = this;
        let records = this.props.records.filter(r => r.attributesByName[this.props.attributeName].type !== T.Missing)
        
        function mapRecord(r: Model.MarRecord): Filters.RangeModel {
            let attr = r.attributesByName[self.props.attributeName];
            return self.props.mapper(attr);
        }

        let numbers: Set<number> = new Set<number>();
        for(let r of records) {
            let m = mapRecord(r);
            numbers.add(m.from.value);
            numbers.add(m.to.value);
        }

        let allValues: Array<Filters.RangeValue> = [];
        for(let r of records) {
            let m = mapRecord(r);

            let from = m.from.value;
            if(numbers.has(from)) {
                numbers.delete(from);
                allValues.push(m.from)
            }

            let to = m.to.value;
            if(numbers.has(to)) {
                numbers.delete(to);
                allValues.push(m.to)
            }
        }

        allValues.sort((a,b) => {
            return b.value - a.value;
        });

        let selectedFrom: number = this.props.params.min;
        let selectedTo: number = this.props.params.max;

        let fromValue: string
        let toValue: string

        if(selectedFrom !== -Infinity) {
            fromValue = selectedFrom.toString();
        }
        
        if(selectedTo !== Infinity) {
            toValue = selectedTo.toString();
        }

        let optionsFrom: Array<JSX.Element> = []
        let optionsTo: Array<JSX.Element> = []
        for(let value of allValues) {
            let option = <Antd.Select.Option value={value.value.toString()} key={value.value.toString()}>{value.display}</Antd.Select.Option>
            if(value.value <= selectedTo) {
                optionsFrom.push(option);
            }
            if(value.value >= selectedFrom) {
                optionsTo.push(option);
            }
        }

        function changeMin(value: string) {
            self.props.onChange({
                min: Number(value),
                max: self.props.params.max
            })
        }

        function changeMax(value: string) {
            self.props.onChange({
                min: self.props.params.min,
                max: Number(value)
            })
        }
        
        return (
            <div>
                <Antd.Select
                    showSearch
                    style={{"width": "46%", "marginRight": "2%", "marginLeft": "2%"}}
                    placeholder={"min."}
                    optionFilterProp="children"
                    onChange={changeMin}
                    value={fromValue}>                    
                    {optionsFrom}
                </Antd.Select>
                <Antd.Select
                    showSearch
                    style={{"width": "46%", "marginRight": "2%", "marginLeft": "2%"}}
                    placeholder={"max."}
                    optionFilterProp="children"
                    onChange={changeMax}
                    value={toValue}>
                    {optionsTo}
                </Antd.Select>
            </div>
        );
    }
}