import * as React from "react";

import { Panel, Table } from "react-bootstrap-typescript";
import * as Model from "../model/Model";
import { RendererFor, RenderCompact } from "../utils/AttributeRenderer";
import * as Links from "../utils/Links";
import * as FormatText from "../utils/FormatText";
import { Checkbox } from "./Checkbox";
import * as Orderings from "../utils/Orderings";

export interface DatabaseTableProps {
    records: Array<Model.MarRecord>
    attributeOrder: Array<string>
    attributeSize: (name: string) => string
    selectedRecords?: Set<string>,
    onSelectChange?: (mmpID: string, selected: boolean) => void
}

export class DatabaseTable extends React.Component<DatabaseTableProps, {}> {
    render() {
        let renderCheckboxes: boolean = !(this.props.selectedRecords === null || typeof this.props.selectedRecords === 'undefined')
        //let renderCheckboxes: boolean = false;
        let clickableColomns: number = 2;

        let headers: Array<JSX.Element> = []
        if(renderCheckboxes) {
            headers.push(<th key="checkbox"></th>)
        }
        for(let name of this.props.attributeOrder) {
            headers.push(<th key={name}><div style={{"minWidth": this.props.attributeSize(name)}}>{Orderings.AttributeLabel(name)}</div></th>);
        }

        let rows: Array<JSX.Element> = []

        for(let rec of this.props.records) {
            let row: Array<JSX.Element> = []
            let attributes: {[key:string]:Model.Attribute} = {}
            for(let attr of rec.attributes) {
                attributes[attr.name] = attr;
            }
            for(let name of this.props.attributeOrder) {
                let attr = attributes[name];
                if((attr.type == Model.AttributeType.Accession && (attr.data as Model.Accession).url) ||
                attr.type == Model.AttributeType.AccessionList || attr.type == Model.AttributeType.PublicationPmid ||
                attr.type == Model.AttributeType.Position) {
                    row.push(
                        <td key={name}>
                            <div
                                style={{"height": "1.5em", "overflow": "hidden"}}>
                                    {RenderCompact(attr, name)}
                            </div>
                        </td>
                    );
                } else {
                    row.push(
                        <td key={name}>
                            <div
                                onClick={() => Links.Record(rec.mmpID).navigate()}
                                style={{"height": "1.5em", "overflow": "hidden"}}>
                                    {RenderCompact(attr, name)}
                            </div>
                        </td>
                    );
                }
            }

            let onSelectChange = function(value: boolean) {
                if(this.props.onSelectChange) {
                    this.props.onSelectChange(rec.mmpID, value)
                }
            }.bind(this);

            let selected = this.props.selectedRecords.has(rec.mmpID) || false;

            if(renderCheckboxes) {
                row.unshift(<td style={{"textAlign": "right"}} key="CHECKBOX"><Checkbox onSelect={onSelectChange} selected={selected} /></td>);
            }
            
            rows.push(<tr key={rec.mmpID}>{row}</tr>);
        }
        
        return (
            <Table striped bordered condensed hover>
                <thead>
                    <tr>
                        {headers}
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </Table>
        );
    }
}