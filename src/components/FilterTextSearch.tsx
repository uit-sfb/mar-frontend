import * as React from "react";
import * as Antd from "antd";



export interface TextSearchProps {
    query: string
    onSearch: (query: string) => void
}

export class FilterTextSearch extends React.Component<TextSearchProps, {}> {
    render() {
        let q = this.props.query
        let self=this;

        // todo textbox
        // todo button

        let handleChangeEvt: React.FormEventHandler<any> = (evt) => {
            self.props.onSearch(evt.target.value);
        };

        return (
            <div>
                <Antd.Input placeholder="Search" onChange={handleChangeEvt} value={q}/>
            </div>
        );
    }
}