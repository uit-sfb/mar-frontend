export interface LocationArgs {
    recordsPage?: number
    filters?: {[name:string]:string}
}
