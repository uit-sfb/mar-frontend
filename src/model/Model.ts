export interface MarDatabase {
    readonly records: Array<MarRecord>
    readonly type: string
    readonly recordsById: {[key:string]:MarRecord}
    readonly defaultAttributesOrder: Array<string>
}

export interface MarRecord {
    readonly attributes: Array<Attribute>
    readonly mmpID: string
    readonly attributesByName: {[key:string]:Attribute}
}

export enum AttributeType {
    String,
    Boolean,
    Missing,
    Rrnas, 
    OtherTyping, 
    PrecisionDecimalStringRange,
    PrecisionDecimalStringFixed,
    Duration,
    Position,
    AccessionList,
    PublicationPmid,
    StringList,
    PrecisionDateTimeFixed,
    PrecisionDateTimeRange,
    Accession
}

export interface Attribute {
    readonly type: AttributeType
    readonly data: AttributeData
    readonly name: string
}

type AttributeData = string | Missing | boolean |
    String |
    StringList |
    Rrnas |
    OtherTyping |
    PrecisionDecimalStringRange |
    Duration |
    Accession |
    Position | 
    StringList |
    AccessionList |
    PublicationPmid |
    PrecisionDateTimeFixed |
    PrecisionDateTimeRange

// Types
// todo: remove
export interface String {
    readonly value: string
}

export interface Missing {
    readonly reason: string
}

export interface Rrnas {
    rrna5S: number
    rrna16S: number
    rrna23S: number
}

export interface OtherTyping {
    typing: string
    term: string
}

export interface PrecisionDecimalStringRange {
    min: string
    max: string
}

export interface Duration {
    amount: String | PrecisionDecimalStringRange
    unit: string
}

export interface Position {
    latitude: number
    longitude: number
}

export interface AccessionList {
    items: Array<Accession>
}

export interface PublicationPmid {
    items: Array<Accession | string>
}

export interface StringList {
    items: Array<string>
}

export interface PrecisionDateTimeFixed {
    year: number
    month?: number
    date?: number
    hours?: number
    minutes?: number
    seconds?: number
    milliseconds?: number
}

export interface PrecisionDateTimeRange {
    from: PrecisionDateTimeFixed
    to: PrecisionDateTimeFixed
}

export interface Accession {
    readonly value: string
    readonly url?: string
}

