export function formatCamelCase2HRText(s: string): string {
    return s
    // insert a space before all caps
    .replace(/([a-z0-9])([A-Z])/g, '$1 $2')
    // uppercase the first character
    .replace(/^./, function(str){ return str.toUpperCase(); })
}