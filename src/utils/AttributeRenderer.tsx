import * as React from "react";
import * as Model from "../model/Model";
import { Popover, OverlayTrigger, Button, ButtonToolbar, Tooltip } from "react-bootstrap-typescript";

export function RenderCompact(attr: Model.Attribute, name: string): JSX.Element {
    return RendererFor(attr, name).compact();
}

export function RenderMedium(attr: Model.Attribute, name: string): JSX.Element { 
    let renderer = RendererFor(attr, name);
    if(renderer.medium != null) {
        return renderer.medium();
    } else {
        return renderer.compact();
    }
}

export function RendererFor(attr: Model.Attribute, name: string): AttributeRenderer {
    let T = Model.AttributeType
    if(attr == null) {
        return bugRenderer(attr, name);
    }
    switch(attr.type) {
        case T.String:
            return stringRenderer(attr);
        case T.Boolean:
            return booleanRenderer(attr);
        case T.Missing:
            return missingRenderer(attr);
        case T.PrecisionDecimalStringFixed:
            return stringRenderer(attr);
        case T.PrecisionDecimalStringRange:
            return lengthRangeRenderer(attr);
        case T.Duration:
            return durationRenderer(attr);
        case T.Accession:
            return accessionRenderer(attr);
        case T.Rrnas:
            return rrnasRenderer(attr);
        case T.OtherTyping:
            return otherTypingRenderer(attr);
        case T.Position:
            return positionRenderer(attr);
        case T.PrecisionDateTimeFixed:
            return precisionDateTimeFixedRenderer(attr.data as Model.PrecisionDateTimeFixed);
        case T.PrecisionDateTimeRange:
            return precisionDateTimeRangeRenderer(attr);
        case T.StringList:
            return stringListRenderer(attr);
        case T.AccessionList:
            return accessionListRenderer(attr);
        case T.PublicationPmid:
            return publicationPmidRenderer(attr); 
        default:
            // This shouldn't happen, but there might be bugs.
            // In case of a bug, we'll handle it gracefully by
            // rendering a placeholder instead of throwing an exception.
            return bugRenderer(attr, name);
    }
}

export interface AttributeRenderer {
    text(): string
    compact(): JSX.Element
    medium?(): JSX.Element
}

function stringRenderer(attr: Model.Attribute): AttributeRenderer {
    let value = (attr.data as Model.String).value
    
    return {
        text() {
            return value;
        },
        compact() {
            return <div>{value}</div>;
        }
    }
}

function booleanRenderer(attr: Model.Attribute): AttributeRenderer {
    let text: string;
    switch(attr.data) {
        case true:
            text = "Yes";
            break;
        case false:
            text = "No";
            break;
        default:
            throw "booleanRenderer: not a boolean: " + attr.data
    }

    return {
        text() {
            return text;
        },
        compact() {
            return <div>{text}</div>
        }
    }
}

function missingRenderer(attr: Model.Attribute): AttributeRenderer {
    let missing = attr.data as Model.Missing;

    let text: string;
    switch(missing.reason) {
        case "notAvailable": // todo: remove (old schema)
        case "notApplicable":
            text = "not applicable";
            break;
        default:
            text = missing.reason;
            break;
    }

    return {
        text() {
            return text;
        },
        compact() {
            return <i>{text}</i>
        }
    }
}

// todo: Rename!
function lengthRangeRenderer(attr: Model.Attribute): AttributeRenderer {
    let min = (attr.data as Model.PrecisionDecimalStringRange).min.toString()
    let max = (attr.data as Model.PrecisionDecimalStringRange).max.toString()
    return {
        text() {
            return min.concat("-").concat(max);
        },
        compact() {
            return <div>{min}-{max}</div>;
        }
    }
}

function durationRenderer(attr: Model.Attribute): AttributeRenderer {
    let sep = "-"
    let amount = ""
    if(((attr.data as Model.Duration).amount as Model.String).value != null) {
        amount = ((attr.data as Model.Duration).amount as Model.String).value
    } else {
        amount = ((attr.data as Model.Duration).amount as Model.PrecisionDecimalStringRange).min +
            sep + ((attr.data as Model.Duration).amount as Model.PrecisionDecimalStringRange).max
    }
    let unit = (attr.data as Model.Duration).unit
    return {
        text() {
            return amount.concat(" ").concat(unit);
        },
        compact() {
            return <div>{amount} {unit}</div>;
        }
    }
}

function rrnasRenderer(attr: Model.Attribute): AttributeRenderer {
    let rrnas = attr.data as Model.Rrnas;
    let text = String(rrnas.rrna5S).concat(", ").concat(String(rrnas.rrna16S)).concat(", ").concat(String(rrnas.rrna23S));
    return {
        text() {
            return text;
        },
        compact() {
            let tooltip = <Tooltip>5S: {rrnas.rrna5S}, 16S: {rrnas.rrna16S}, 23S: {rrnas.rrna23S}</Tooltip>
            return (
                <OverlayTrigger placement="left" overlay={tooltip}>
                    <div>{rrnas.rrna5S}, {rrnas.rrna16S}, {rrnas.rrna23S}</div>
                </OverlayTrigger>
            ); 
        }
    }
}
function otherTypingRenderer(attr: Model.Attribute): AttributeRenderer {
    let ot = attr.data as Model.OtherTyping;
    let text = ot.typing.concat(":").concat(ot.term);

    return {
        text() {
            return text;
        },
        compact() {
            let tooltip = "typing: " + ot.typing+ ", term: "+ot.term;
            return <div data-toggle="tooltip" title={tooltip}>
                {ot.typing}:{ot.term}
            </div>
        }
    }
}

function positionRenderer(attr: Model.Attribute): AttributeRenderer {
    let pos = attr.data as Model.Position;
    let latlon = +pos.latitude+","+pos.longitude
    let mapUrl ="http://www.google.com/maps/place/"+latlon+"/@"+latlon+",1z";
    
    return {
        text() {
            return latlon;
        },
        compact() {
            return <a href={mapUrl}>{pos.latitude},{pos.longitude}</a>
        }
    }
}

function precisionDateTimeFixedRenderer(data: Model.PrecisionDateTimeFixed): AttributeRenderer {
    let dateTimeText = data.year.toString()
    let dateSep = "-"
    let space = " "
    let timeSep = ":"

    function twoDecimals(num: number): string {
        if(num < 10) {
            return "0".concat(num.toString())
        } else {
            return num.toString()
        }
    }

    if(data.month != null && data.month != 0) {
        dateTimeText += dateSep
        dateTimeText += twoDecimals(data.month);
        if(data.date != null && data.date != 0) {
            dateTimeText += dateSep
            dateTimeText += twoDecimals(data.date);
            if(data.hours != null && data.minutes != null && data.seconds != null && data.milliseconds != null) {
                dateTimeText += space;
                dateTimeText += twoDecimals(data.hours) + timeSep;
                dateTimeText += twoDecimals(data.minutes) + timeSep;
                dateTimeText += twoDecimals(data.seconds);
            }
        }
    }
    return {
        text() {
            return dateTimeText;
        },
        compact() {
            return <div>{dateTimeText}</div>;
        }
    }
}

function precisionDateTimeRangeRenderer(attr: Model.Attribute): AttributeRenderer {
    let T = Model.AttributeType
    let dateTextFrom = precisionDateTimeFixedRenderer((attr.data as Model.PrecisionDateTimeRange).from as Model.PrecisionDateTimeFixed)
    let dateTextTo = precisionDateTimeFixedRenderer((attr.data as Model.PrecisionDateTimeRange).to as Model.PrecisionDateTimeFixed)
    let text = dateTextFrom.text().concat(" - ").concat(dateTextTo.text());

    return {
        text() {
            return text;
        },
        compact() {
            return <div>{dateTextFrom.compact().props.children}--{dateTextTo.compact().props.children}</div>;
        }
    }
}

function stringListRenderer(attr: Model.Attribute): AttributeRenderer {
    let attribute = attr.data as Model.StringList;
    let items = attribute.items
    let head: string
    let all: string
    if (items.length > 1) {
       head = items[0] + ", ..."
       all = items.join(", ")
    } else if (items.length == 1) {
        head = all = items[0]    
         
    } else if (items.length ==0) {
        head = all = "" 
    }
    
    return {
        text() {
            return all;
        },
        compact() {
            return <div data-toggle="tooltip" title={all}>
                {head}
            </div>
        },
        medium() {
           return <div>{all}</div>
        }
    }
}

function accessionRenderer(attr: Model.Attribute): AttributeRenderer {
    let accession = attr.data as Model.Accession;

    return {
        text() {
            return accession.value;
        },
        compact() {
            if(accession.url) {
                return <a href={accession.url} target="_blank">{accession.value}</a>
            } else {
                return <div>{accession.value}</div>
            }
        }
    }
}

function accessionListRenderer(attr: Model.Attribute): AttributeRenderer {
    let attribute = attr.data as Model.AccessionList;
    let items = attribute.items
    let strItems:Array<string> = []
    for (let i=0;i<items.length; i++){
        strItems.push(items[i].value)
    }
    let head: string
    let all: string
    if (strItems.length > 1){
       head = strItems[0] + ", ..."
       all = strItems.join(", ")
    }else if (strItems.length == 1){
        head = all = strItems[0]    
         
    }else if (strItems.length ==0){
        head = all = "" 
    }

    //medium, not just strings
    let links: Array<JSX.Element> =[]
    var first = true
    for(let i=0; i<items.length;i++){
        let cur: JSX.Element
        if(items[i].url) {
                cur = <a href={items[i].url} target="_blank">{items[i].value}</a>
            } else {
                cur = <div>{items[i].value}</div>
            }
        if(first === false) {
            links.push(<span>,&nbsp;</span>);
        }
        links.push(cur)
        first = false;
    }
    
    return {
        text() {
            return ""; // TODO
        },
        compact() {
            return <div data-toggle="tooltip" title={all}>
                {head}
            </div>
        },
        medium() {
           return <div>{links}</div>
        }
    }
}

function publicationPmidRenderer(attr: Model.Attribute): AttributeRenderer {
    let data = attr.data as Model.PublicationPmid
    let toolTipValue = ""

    let elements: Array<JSX.Element> = []
    let separator = ""
    let firstIter = true
    for(let item of data.items) {
        if (typeof item === 'string' || item instanceof String) {
            let url = item as string
            elements.push(<div>{separator}<a href={url} target="_blank">{url}</a></div>)
            toolTipValue = toolTipValue.concat(separator, url);
        } else {
            let accession = item as Model.Accession
            if(!firstIter) {
                elements.push(<span>&nbsp;</span>)
            }
            if(accession.url) {
                elements.push(<a href={accession.url} target="_blank">{accession.value}</a>)
            } else {
                elements.push(<span key={accession.value}>{accession.value}</span>)
            }
            
            toolTipValue = toolTipValue.concat(separator, accession.value);
        }
        firstIter = false
        separator = ", "
    }

    return {
        text() {
            return ""; // TODO
        },
        compact() {
            if(elements.length <= 1) {
                return <div>{elements}</div>
            } else {
                <i data-toggle="tooltip" title={toolTipValue}>{toolTipValue.substring(0,8)}...</i>
            }
        },

        medium(): JSX.Element {
            return <div>{elements}</div>;
        }
    }
}

function bugRenderer(attr: Model.Attribute, name: string): AttributeRenderer {
    console.warn("bugRenderer was called on the following attribute:", attr, ", attribute name:", name);
    return {
        text() {
            return "???";
        },
        compact() {
            return <div style={{"color": "red"}}>???</div>; // todo: make some tooltip, report bug, etc..
        }
    }
}