import { MarDatabase, AttributeType, String } from "../model/Model"

export interface DatabaseStatistics {
    //readonly investigationType: {[key:string]:number}
    readonly numRecords: number
}

export function DatabaseStatistics(db: MarDatabase): DatabaseStatistics {
    /*
    let investigationType: {[key:string]:number} = {}
    for(let record of db.records) {
        for(let attr of record.attributes) {
            if(attr.name === "investigationType") {
                if(attr.type === AttributeType.String) {
                    let value = (attr.data as String).value
                    if(investigationType[value]) {
                        investigationType[value]++;
                    } else {
                        investigationType[value] = 1;
                    }
                }
            }
        }
    }
    */

    return {
        //investigationType: investigationType,
        numRecords: db.records.length
    }
}