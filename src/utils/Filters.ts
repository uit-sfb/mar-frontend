import * as Model from "../model/Model";
import * as MathUtils from "./MathUtils";
import * as AttributeRenderer from "./AttributeRenderer";

let T = Model.AttributeType;

export type RecordFilter = (recs: Array<Model.MarRecord>) => Array<Model.MarRecord>

export function RecordFilter<FilterParams>(filter: AttributeFilter<FilterParams>, params: FilterParams, attributeName: string): RecordFilter {
    return function(recs: Array<Model.MarRecord>): Array<Model.MarRecord> {
        let result: Array<Model.MarRecord> = []

        for(let rec of recs) {
            let filterArgs: AttributeFilterArgs<FilterParams> = {
                attribute: rec.attributesByName[attributeName],
                params: params
            };
            if(filter(filterArgs)) {
                result.push(rec);
            }
        }

        return result;
    }
}

export function FilterRecords(filters: Array<RecordFilter>, records: Array<Model.MarRecord>): Array<Model.MarRecord> {
    var result: Array<Model.MarRecord> = records;

    for(let filter of filters) {
        result = filter(result);
    }

    return result;
}


export interface TextFilterParams {
    query: string
}

export function TextFilter(params: TextFilterParams): RecordFilter {
    return function(recs: Array<Model.MarRecord>): Array<Model.MarRecord> {
        let res: Array<Model.MarRecord> = [];

        if (params.query === "") {
            return recs;
        }

        for(let rec of recs) {
            for(let attr of rec.attributes) {
                let renderer = AttributeRenderer.RendererFor(attr, attr.name)
                let attrText = renderer.text().toString().toLowerCase();
                let queryStr = params.query.toLowerCase();
                if(attrText.indexOf(queryStr) !== -1) {
                    res.push(rec);
                    break;
                }
            }
        }

        return res;
    }
}

//////////////////////

export interface RangeModel {
    from: RangeValue
    to: RangeValue
}

export interface RangeValue {
    value: number
    display: string
}

export type AttributeRangeMapper = (attr: Model.Attribute) => RangeModel

//////////////////////

export interface AttributeFilterArgs<FilterParams> {
    attribute: Model.Attribute
    params: FilterParams
}


export type AttributeFilter<FilterParams> = (arg: AttributeFilterArgs<FilterParams>) => boolean


/////////////////////// IMPL




export interface StringFilterParams {
    equals: string
}

export let StringFilter: AttributeFilter<StringFilterParams> = (arg: AttributeFilterArgs<StringFilterParams>) => {
    if(arg.attribute.type !== Model.AttributeType.String && arg.attribute.type !== Model.AttributeType.Accession) {
        return false
    }

    let data: Model.String = arg.attribute.data as Model.String;

    return data.value === arg.params.equals;
}

export interface StringFilterStringListParams {
    equals: string
}

export let StringFilterStringList: AttributeFilter<StringFilterStringListParams> = (arg: AttributeFilterArgs<StringFilterStringListParams>) => {
    if(arg.attribute.type !== Model.AttributeType.StringList && arg.attribute.type !== Model.AttributeType.AccessionList) {
        return false
    }
    let data: Model.StringList = arg.attribute.data as Model.StringList;
    let res = false
    for(let item of data.items)
    {
        if(item === arg.params.equals)
        {
            res = true;
            break;
        }
    }
    return res;
}

export interface PrecisionDecimalStringFilter {
    
}

export interface RangeFilterParams {
    min: number
    max: number
}


export let PrecicionDecimalStringFilter: AttributeFilter<RangeFilterParams> = (arg: AttributeFilterArgs<RangeFilterParams>) => {
    let query = new MathUtils.NumberRange(arg.params.min, arg.params.max);
    let range: MathUtils.NumberRange;

    switch(arg.attribute.type) {
        case T.Missing:
            return false;
        case T.PrecisionDecimalStringFixed:
            {
                let data = arg.attribute.data as Model.String
                let value = Number(data.value);
                range = new MathUtils.NumberRange(value, value);
            }
        break;
        case T.PrecisionDecimalStringRange:
            {
                let data = arg.attribute.data as Model.PrecisionDecimalStringRange
                range = new MathUtils.NumberRange(Number(data.min), Number(data.max));
            }
        break;
        default:
            throw "Invalid AttributeType."
    }
    return query.overlaps(range);
}

export let YearFilter: AttributeFilter<RangeFilterParams> = (arg: AttributeFilterArgs<RangeFilterParams>) => {
    let query = new MathUtils.NumberRange(arg.params.min, arg.params.max);
    let range: MathUtils.NumberRange;

    let attr = arg.attribute;

    switch(arg.attribute.type) {
        case T.Missing:
            return false;
        case T.PrecisionDateTimeFixed:
        {
            let data = attr.data as Model.PrecisionDateTimeFixed;
            let year = Number(data.year);
            range = new MathUtils.NumberRange(year, year);
        }
        break;
        case T.PrecisionDateTimeRange:
        {
            let data = attr.data as Model.PrecisionDateTimeRange;
            let fromYear = Number(data.from.year)
            let toYear = Number(data.to.year);
            range = new MathUtils.NumberRange(fromYear, toYear);
        }
        break;
        default:
            throw "Invalid AttributeType."
    }
    return query.overlaps(range);
}


//////// Range mappers

export let YearRangeMapper: AttributeRangeMapper = (attr: Model.Attribute): RangeModel => {
    let model: RangeModel;

    switch(attr.type) {
        case T.PrecisionDateTimeFixed:
            {
                let data = attr.data as Model.PrecisionDateTimeFixed;
                let value = {
                    value: data.year,
                    display: data.year.toString()
                };
                model = {
                    from: value,
                    to: value
                }
            }
        break;
        case T.PrecisionDateTimeRange:
            {
                let data = attr.data as Model.PrecisionDateTimeRange;
                let fromYear = data.from.year;
                let toYear = data.to.year;
                model = {
                    from: {
                        value: fromYear,
                        display: fromYear.toString()
                    },
                    to: {
                        value: toYear,
                        display: toYear.toString()
                    }
                }
            }
        break;
        default:
            throw "Invalid type in YearMapper";
    }
    return model;
}

export function DecimalUnitRangeMapper(unit: string): AttributeRangeMapper {
    return (attr: Model.Attribute) => {
        let model: RangeModel

        function u(value: string): RangeValue {
            return {
                value: Number(value),
                display: value + " " + unit
            }
        }

        switch(attr.type) {
            case T.PrecisionDecimalStringFixed:
            {
                let data = attr.data as Model.String;
                model = {
                    from: u(data.value),
                    to: u(data.value)
                }
            }
            break;
            
            /*
            case T.Duration:
            {
                let year = attr.data.amount.value as String;
                console.log(year)
                model = {
                    from: u(year),
                    to: u(year)
                }
            }
            break;
            */
            
            case T.PrecisionDecimalStringRange:
            {
                let data = attr.data as Model.PrecisionDecimalStringRange;
                model = {
                    from: u(data.min),
                    to: u(data.max)
                }
            }
            break;

            default:
                throw "Invalid type in DecimalUnitRangeMapper";
        }

        return model;
    }
}