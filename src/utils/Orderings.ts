export interface Category {
    readonly heading: string
    readonly attributeNames: Array<string>
    readonly databaseType?: string
    readonly expanded: boolean
    readonly bsStyle?: string
}

let mainCategory: Category = {
    heading: "Summary",
    databaseType: "marref mardb marfun marcat saldb",
    expanded: false,
    bsStyle: "info",
    attributeNames: [
        "mmpID",
        "fullScientificName",
        "strain",
        "typeStrain",
        "geoLocName",
        "collectionDate",
        "biosampleAccession",
        "bacdiveId",
        "cultureCollection",
        "isolationCountry",
        "envPackage",
        "isolationSource",
        "hostScientificName",
        "curationDate",
        "updatedDate",
        "implementationDate",
        "microbePackage",
        "investigationType",
        "bioprojectAccession",
        "genbankAccession",
        "ncbiTaxonIdentifier",
        "refBiomaterial",
        "silvaAccessionSSU",
        "silvaAccessionLSU",
        "uniprotProteomeID",
        "publicationPmid",
        "comments",
        "genomeStatus",
        "sra",
        "runId",
        "microbePackageId",
    ]
}

let marCatTableColumns: Array<string> = [
    "mmpID",
    "bioprojectAccession",
    "biosampleAccession",
    "envBiome",
    "geoLocName",
    "latLon",
    "collectionDate",
    "sampleName",
]

let marRefColumns: Array<string> = [
    "mmpID",
    "fullScientificName",
    "strain",
    "typeStrain",
    "geoLocName",
    "collectionDate",
    "biosampleAccession",
    "bacdiveId",
]

let marFunColumns: Array<string> = [
    "mmpID",
    "fullScientificName",
    "strain",
    "typeStrain",
    "geoLocName",
    "collectionDate",
    "biosampleAccession",
    "itsonedbId",
]

let sarsCoV2DBColumns: Array<string> = [
    "cdbId",
    "genbankAccession",
    "isolationCountry",
    "collectionDate",
    "hostScientificName",
    "sampCollectDevice",
    "isolationSource",
    "genomeLength",
    "quality",
]

let samplingInfo: Category = {
    heading: "Sampling info",
    expanded: false,
    databaseType: "marref mardb marfun marcat saldb",
    attributeNames: [
        "sampleName",
        "sampCollectDevice",
        "samplingCampaign",
        "samplingSite",
        "collectionTime",
        "sampleProtocolLabel",
        "sampleLowerThreshold",
        "sampleUpperThreshold",
        "eventDateTime",
        "eventDevice",
        "contactOrganisation",
        "contactName",
        "contactEmail",
    ]
};

let categories = [
        mainCategory,
        {
            heading: "Organism and taxon info",
            expanded: false,
            databaseType: "marref mardb saldb",
            attributeNames: [
                "fullScientificName",
                "organism",
                "taxonLineageNames",
                "taxonLineageIds",
                "ncbiTaxonIdentifier",
                "strain",
                "kingdom",
                "phylum",
                "class",
                "order",
                "family",
                "genus",
                "species",
                "gtdbClassification",
                "fastaniReference",
                "fastaniAni"
            ]
        },
        {
            heading: "Organism and taxon info",
            expanded: false,
            databaseType: "marfun marcat",
            attributeNames: [
                "fullScientificName",
                "organism",
                "taxonLineageNames",
                "taxonLineageIds",
                "ncbiTaxonIdentifier",
                "strain",
                "kingdom",
                "phylum",
                "class",
                "order",
                "family",
                "genus",
                "species"
            ]
        },
        {
            heading: "Isolate info",
            databaseType: "marref mardb marfun",
            expanded: false,
            attributeNames: [
                "investigationType",
                "sampleType",
                "isolationSource",
                "collectionDate",
                "altElev",
                "depth",
                "envSalinity",
                "envTemp",
                "geoLocName",
                "latLon",
                "isolationCountry",
                "envBiome",
                "envFeature",
                "envMaterial",
                "envPackage",
                "isolGrowthCondt",
                "refBiomaterial",
                "cultureCollection",
                "collectedBy",
                "biosampleAccession",
                "isolationComments",
                "projectName",
                "geoLocNameGaz",
                "geoLocNameGazEnvo",
                "analysisProjectType",

                "enaVersion",
                "classificationLineage",
                "classificationId",
                "chlorophyll",
                "silicate",
                "dissOxygen",
                "nitrate",
                "phosphate",
            ]
        },
        {
            heading: "Isolate info",
            databaseType: "marcat",
            expanded: false,
            attributeNames: [
                "investigationType",
                "sampleType",
                "isolationSource",
                "collectionDate",
                "altElev",
                "depth",
                "envSalinity",
                "envTemp",
                "geoLocName",
                "latLon",
                "isolationCountry",
                "envBiome",
                "envFeature",
                "envMaterial",
                "envPackage",
                "isolGrowthCondt",
                "refBiomaterial",
                "cultureCollection",
                "collectedBy",
                "biosampleAccession",
                "isolationComments",
                "projectName",
                "microbePackageId",
                "enaVersion",
                "classificationLineage",
                "classificationId",
                "geoLocNameGaz",
                "geoLocNameGazEnvo",
                "chlorophyll",
                "silicate",
                "dissOxygen",
                "nitrate",
                "phosphate",
                "hostCommonName",
            ]
        },
        {
            heading: "Isolate info",
            databaseType: "sarscov2db",
            expanded: false,
            attributeNames: [
                "envPackage",
                "sampCollectDevice",
                "investigationType",
                "isolateName",
                "isolationSource",
                "collectionDate",
                "geoLocName",
                "isolationCountry",
                "latLon",
                "isolGrowthCondt",
                "envTemp",
                "refBiomaterial",
                "collectedBy",
                "biosampleAccession",
                "isolationComments",
                "projectName",
                "geoLocNameGaz",
                "geoLocNameGazEnvo",
            ]
        },
        samplingInfo,
        {
            heading: "Host and pathogenicity info",
            databaseType: "marref mardb marfun",
            expanded: false,
            attributeNames: [
                "hostCommonName",
                "hostScientificName",
                "hostSex",
                "hostHealthStage",
                "hostAge",
                "pathogenicity",
                "disease",
                "bodySampleSite",
                "bodySampleSubSite",
                "otherClinical"
            ]
        },
        {
            heading: "Host info",
            databaseType: "sarscov2db",
            expanded: false,
            attributeNames: [
                "hostCommonName",
                "hostScientificName",
                "hostSex",
                "hostHealthStage",
                "hostAge",
                "bodySampleSite",
                "bodySampleSubSite",
                "otherClinical"
            ]
        },
        {
            heading: "Assembly info",
            expanded: false,
            databaseType: "marref mardb marfun marcat saldb",
            attributeNames: [
                "sequencingCenters",
                "seqMeth",
                "sequencingDepth",
                "assemblyAccessionGenbank",
                "assemblyAccessionRefseq",
                "assembly",
                "assemblyVersion",
                "gcContent",
                "contigs",
                "n50",
                "numReplicons",
                "genomeLength",
                "plasmids",
                "binning",
                "binningVersion",
                "estimatedCompleteness",
                "estimatedContamination",
                "strainHeterogeneity",
                "qs",
                "mapping",
                "mappingVersion",
                "qualityAssessment",
                "qualityAssessmentVersion"
            ]
        },
        {
            heading: "Assembly info",
            expanded: false,
            databaseType: "sarscov2db",
            attributeNames: [
                "submittedBy",
                "seqMeth",
                "sequencingDepth",
                "assemblyAccessionGenbank",
                "assemblyAccessionRefseq",
                "assemblyTool",
                "assemblyVersion",
                "submissionDate",
                "contigs",
                "n50"
            ]
        },
        {
            heading: "Annotation info",
            expanded: false,
            databaseType: "marref mardb marfun marcat saldb",
            attributeNames: [
                "annotationProvider",
                "annotationDate",
                "annotationPipeline",
                "annotationMethod",
                "annotationSoftwareRevision",
                "featuresAnnotated",
                "refseqCds",
                "ncbiRefseqAccession",
                "genes",
                "cds",
                "pseudoGenes",
                "rrnas",
                "completeRrnas",
                "partialRrnas",
                "totalTrnas",
                "uniqueTrnas",
                "ncrna",
                "frameshiftedGenes"
            ]
        },
        {
            heading: "Genome info", // (ex. Annotation info)
            expanded: false,
            databaseType: "sarscov2db",
            attributeNames: [
                "quality",
                "genomeStatus",
                "dnaShape",
                "numSegments",
                "genomeLength",
                "gcContent",
                "genes",
                "cds",
                "pseudoGenes",
                "frameshiftedGenes"
            ]
        },
        {
            heading: "Virus and taxon info",
            expanded: false,
            databaseType: "sarscov2db",
            attributeNames: [
                "morphology",
                "baltimoreClass",
                "fullScientificName",
                "organism",
                "taxonLineageNames",
                "taxonLineageIds",
                "ncbiTaxonIdentifier",
                "kingdom",
                "family",
                "genus",
                "species",
                "otherTyping",
                "pathogenicity"
            ]
        },
        {
            heading: "Phenotype info",
            expanded: false,
            databaseType: "marref mardb marfun marcat saldb",
            attributeNames: [
                "gramStain",
                "cellShape",
                "motility",
                "sporulation",
                "temperatureRange",
                "optimalTemperature",
                "halotolerance",
                "oxygenRequirement",
                "biovar",
                "serovar",
                "pathovar",
                "otherTyping",
                "typeStrain",
                "ploidy",
                "propagation"
            ]
        },
        {
            heading: "Secondary metabolities",
            expanded: false,
            attributeNames: [
                "antismashTypes",
                "antismashClusters",
                "chebiId",
                "chebiName",
                "chemblId",
                "compoundName",
                "uniprotId",
                "uniprotDescription"
            ]
        },
        {
            heading: "Project",
            expanded: false,
            databaseType: "marref mardb marfun",
            attributeNames: [
                "mmpID",
                "bacdiveId",
                "itsonedbId",
                "curationDate",
                "updatedDate",
                "implementationDate",
                "microbePackage",
                "bioprojectAccession",
                "genbankAccession",
                "silvaAccessionSSU",
                "silvaAccessionLSU",
                "uniprotProteomeID",
                "publicationPmid",
                "comments",
                "baseID",
                "genomeStatus",
                "mmpBiome",
            ]
        },
        {
            heading: "Project",
            expanded: false,
            databaseType: "sarscov2db",
            attributeNames: [
                "cdbId",
                "implementationDate",
                "curationDate",
                "updatedDate",
                "microbePackage",
                "publicationPmid",
                "genbankAccession",
                "bioprojectAccession",
                "sraRun",
                "gisaidId",
                "cngbdbAccession",
                "nmcdAccession",
                "gwAccession"
            ]
        }
    ];

let attributeCompactSize: {[name:string]:string} = {
    "mmpID": "8em",
    "fullScientificName": "25em",
    "hostScientificName": "25em"
};

let databaseLabels: {[name:string]:string} = {
    "marref": "MarRef",
    "mardb": "MarDB",
    "marcat": "MarCat",
    "marfun": "MarFun",
    "saldb": "SalDB",
    "sarscov2db": "SarsCoV2DB"
}

let attributeLabels: {[name:string]:string} = {
    "mmpID": "MMP ID",
    "bacdiveId": "Bacdive ID",
    "itsonedbId": "ITSoneDB identifier",
    "curationDate": "Curation Date",
    "updatedDate": "Updated Date",
    "implementationDate": "Implementation Date",
    "microbePackage": "Microbe Package",
    "bioprojectAccession": "ENA BioProject accession identifier",
    "genbankAccession": "ENA GenBank nucleotide accession identifier",
    "silvaAccessionSSU": "Silva Accession SSU",
    "silvaAccessionLSU": "Silva Accession LSU",
    "uniprotProteomeID": "Uniprot Proteome ID",
    "publicationPmid": "Publications",
    "fullScientificName": "Full Scientific Name",
    "organism": "Organism",
    "taxonLineageNames": "NCBI Taxon Lineage names",
    "taxonLineageIds": "NCBI Taxon lineage ids",
    "ncbiTaxonIdentifier": "NCBI Taxon Identifier",
    "strain": "Strain",
    "kingdom": "NCBI Kingdom",
    "phylum": "NCBI Phylum",
    "class": "NCBI Class",
    "order": "NCBI Order",
    "family": "NCBI Family",
    "genus": "NCBI Genus",
    "species": "NCBI Species",
    "gtdbClassification": "GTDB Taxon Lineage Names",
    "fastaniReference": "GTDB reference genome",
    "fastaniAni": "GTDB reference ANI",
    "investigationType": "Investigation Type",
    "sampleType": "Sample Type",
    "isolationSource": "Isolation Source",
    "collectionDate": "Collection Date",
    "altElev": "Geographic location (altitude/elevation) (m)",
    "depth": "Depth (m)",
    "envSalinity": "Environmental Salinity (psu)",
    "envTemp": "Environmental Temperature (°C)",
    "geoLocName": "Geographic location",
    "latLon": "Latitude and Longitude",
    "isolationCountry": "Isolation Country",
    "envBiome": "Environment Biome",
    "envFeature": "Environment Feature",
    "envMaterial": "Environment Material",
    "envPackage": "Environmental Package",
    "isolGrowthCondt": "Isolation and growth condition",
    "refBiomaterial": "Reference for Biomaterial",
    "cultureCollection": "Culture Collection(s)",
    "collectedBy": "Collected By",
    "biosampleAccession": "ENA BioSample accession identifier",
    "isolationComments": "Isolation Comments",
    "projectName": "Project Name",
    "hostCommonName": "Host Common Name",
    "hostScientificName": "Host Scientific Name",
    "hostSex": "Host Sex",
    "hostHealthStage": "Host Health Stage",
    "hostAge": "Host Age",
    "pathogenicity": "Pathogenicity",
    "disease": "Disease",
    "bodySampleSite": "Body Sample Site",
    "bodySampleSubSite": "Body Sample Subsite",
    "otherClinical": "Other Clinical",
    "sequencingCenters": "Sequencing center",
    "submittedBy": "Submitted by",
    "seqMeth": "Sequencing method",
    "sequencingDepth": "Sequencing Depth (x)",
    "assemblyAccessionGenbank": "ENA Assembly accession identifier",
    "assemblyAccessionRefseq": "RefSeq Assembly accession identifier",
    "gcContent": "GC Content (%)",
    "assembly": "Assembly",
    "assemblyVersion": "Assembly tool version",
    "numReplicons": "Number of Replicons",
    "contigs": "Number of Contigs",
    "n50": "N50",
    "genomeLength": "Genome Length (bp)",
    "plasmids": "Plasmids",
    "binning": "Binning tool",
    "binningVersion": "Binning tool version",
    "estimatedCompleteness": "Estimated completeness (%)",
    "estimatedContamination": "Estimated contamination (%)",
    "strainHeterogeneity": "Strain heterogeneity",
    "qs": "Quality score",
    "mapping": "Mapping tool",
    "mappingVersion": "Mapping tool version",
    "qualityAssessment": "Quality assessment tool",
    "qualityAssessmentVersion": "Quality assessment tool version",
    "annotationProvider": "Annotation Provider",
    "annotationDate": "Annotation Date",
    "annotationPipeline": "Annotation Pipeline",
    "annotationMethod": "Annotation Method",
    "annotationSoftwareRevision": "Annotation Software Revision",
    "featuresAnnotated": "Features Annotated",
    "refseqCds": "Refseq CDS",
    "ncbiRefseqAccession": "NCBI Refseq accession identifier",
    "genes": "Genes",
    "cds": "CDS",
    "pseudoGenes": "Pseudo Genes",
    "rrnas": "rRNAs (5S, 16S, 23S)",
    "completeRrnas": "Complete rRNAs (5S, 16S, 23S)",
    "partialRrnas": "Partial rRNAs (5S, 16S, 23S)",
    "totalTrnas": "Total tRNAs",
    "uniqueTrnas": "Unique tRNAs",
    "ncrna": "ncRNAs",
    "frameshiftedGenes": "Frameshifted Genes",
    "gramStain": "Gram Stain",
    "cellShape": "Cell Shape",
    "motility": "Motility",
    "sporulation": "Sporulation",
    "temperatureRange": "Temperature Range",
    "optimalTemperature": "Optimal Temperature",
    "halotolerance": "Halotolerance",
    "oxygenRequirement": "Oxygen Requirement",
    "biovar": "Biovar",
    "serovar": "Serovar",
    "pathovar": "Pathovar",
    "otherTyping": "Other Typing",
    "typeStrain": "Type Strain",
    "ploidy": "Ploidy",
    "propagation": "Propagation",

    // Summary
    "sra": "Sequence Read Archive",
    "runId": "Run ID",
    "comments": "Comments",
    "genomeStatus": "Genome status",

    // isolate info
    "microbePackageId": "Microbe Package (ENA)",
    "enaVersion": "Microbe Package Version (ENA)",
    "classificationLineage": "NCBI Classification Lineage",
    "classificationId": "NCBI Classification ID",
    "geoLocNameGaz": "Geographic Location (GAZ)",
    "geoLocNameGazEnvo": "Geographic Location (GAZ)",
    "analysisProjectType": "Analysis project type",
    "chlorophyll": "Chlorophyll (mg/m3)",
    "silicate": "Silicate (µmol/L)",
    "dissOxygen": "Dissolved Oxygen (µmol/kg)",
    "nitrate": "Nitrate (µmol/L)",
    "phosphate": "Phosphate (µmol/L)",

    // Sampling information
    "sampleName": "Sample Name",
    "sampCollectDevice": "Sample Collection Device",
    "samplingCampaign": "Sampling Campaign/Cruize",
    "samplingSite": "Sampling Site/Station",
    "collectionTime": "Collection time",
    "sampleProtocolLabel": "Protocol Label",
    "sampleLowerThreshold": "Lower Mesh Filter Size",
    "sampleUpperThreshold": "Upper Mesh Filter Size",
    "eventDateTime": "Event Time Start/End",
    "eventDevice": "Event Device",
    "contactOrganisation": "Submitter",
    "contactName": "Submitter",
    "contactEmail": "Submitter",

    // Secondary metabolities
    "antiSMASH": "antiSMASH",
    "antismashTypes": "antiSMASH types",
    "antismashClusters": "antiSMASH clusters",
    "chebiId": "ChEBI Identifier",
    "chebiName": "ChEBI Name",
    "chemblId": "ChEMBL Identifier",
    "compoundName": "ChEMBL Compound name",
    "uniprotId": "Uniprot Identifier",
    "uniprotDescription": "Uniprot Description",
    
    // Project (remaining)
    "baseID": "Base ID",
    "mmpBiome": "MMP biome",
    
    // SarsCoV2DB
    "cdbId": "CoronaDB ID",
    "gisaidId": "GISAID identifier accession",
    "sraRun": "SRA run identifier accession",
    "cngbdbAccession": "CNGBdb Accession",
    "nmcdAccession": "NMCD Accession",
    "gwAccession": "Genome Warehouse Accession",
    "isolateName": "Isolate name",
    "submissionDate": "Submission date",
    "quality": "Sequence quality",
    "dnaShape": "DNA shape",
    "baltimoreClass": "Baltimore classification",
    "morphology": "Morphology",
    "numSegments": "Number of segments",
    "assemblyTool": "Assembly tools"
};

export function AttributeLabel(name: string): string {
    return attributeLabels[name];
}

export function DatabaseLabel(name: string): string {
    return databaseLabels[name];
}

export function AttributeCompactSize(name: string): string {
    let size = attributeCompactSize[name];
    return size || "8em";
}

export function Categories(): Array<Category> {
    return categories;
}

export function MainCategory(): Category {
    return mainCategory;
}

export function TableColumns(databaseType: string): Array<string> {
    switch (databaseType) {
        case "marcat":
            return marCatTableColumns;
        case "mardb":
            return marRefColumns;
        case "marfun":
            return marFunColumns;
        case "marref":
            return marRefColumns;
        case "saldb":
            return marRefColumns;
        case "sarscov2db":
            return sarsCoV2DBColumns;
    }
    throw "TableColumns: invalid databaseType: " + databaseType;
}