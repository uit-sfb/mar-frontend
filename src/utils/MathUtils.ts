export class NumberRange {
    readonly min: number
    readonly max: number

    constructor(min: number, max: number) {
        this.min = min;
        this.max = max;
    }

    overlaps(other: NumberRange): boolean {
        if((this.max >= other.min) && (this.max <= other.max)) {
            return true;
        } else if((this.min >= other.min) && (this.min <= other.max)) {
            return true;
        } else if ((this.min <= other.min) && (this.max >= other.max)) {
            return true;
        }
        return false;
    }
}