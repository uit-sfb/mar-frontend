export interface Link {
    href: string
    navigate(): void
}

function Link(path: string): Link {
    let href = "#" + path

    return {
        href: href,
        navigate() {
            window.location.href = href
        }
    }
}

export function Record(mmpID: string): Link {
    return Link("/records/" + mmpID);
}