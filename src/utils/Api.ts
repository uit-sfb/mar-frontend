import * as $ from "jquery";
import * as Model from "../model/Model";
import * as Xml from "../integration/Xml";
import {convertString} from 'xml2o';
import * as Xml2O from 'xml2o';
var XmlParser = require('fast-xml-parser');
var JSZip = require("jszip");

let xsd: Xml2O.Node
let db: Object
let zipPromise: any
let dbTypePromise: any
let dbProcessingPromise: any
let xsdPromise: JQueryPromise<void>
let xsdParsePromise: Promise<void>
let dbPromise: JQueryPromise<void>
let isJsonDB: Boolean
let isZippedDB: Boolean
let parseDatabase: any
let start = performance.now();

export function GetDatabase(): Promise<Model.MarDatabase> {

    $.ajax({
        url:'data/data.json.zip',
        type:'HEAD',
        async: false,
        processData: false,
        contentType: false,
        success: function()
        {
            isZippedDB = true
            isJsonDB = true
        },
        error: function()
        {
            $.ajax({
                url:'data/data.xml.zip',
                type:'HEAD',
                async: false,
                processData: false,
                contentType: false,
                success: function()
                {
                    isZippedDB = true
                    isJsonDB = false
                },
                error: function()
                {
                    isZippedDB = false
                    dbTypePromise = $.ajax({
                        url:'data/data.json',
                        type:'HEAD',
                        async: false,
                        dataType: "json",
                        success: function()
                        {
                            isJsonDB = true
                        },
                        error: function()
                        {
                            isJsonDB = false
                        }
                    });
                }
            })
        }
    })
    console.log("DB is JSON: " + isJsonDB)
    console.log("DB is Zipped: " + isZippedDB)

    if(isZippedDB) {
        dbPromise = $.ajax({
            url: "data/data.json.zip",
            //dataType: "binary",
            //async: false,
            xhr:function() {
                var xhr = new XMLHttpRequest();
                xhr.responseType = 'arraybuffer'
                return xhr;
            },
            error: function(jqxhr, textStatus, errorThrown) {
                console.log(jqxhr);
                console.log(textStatus);
                console.log(errorThrown);
            }
        }).then(function(value) {
            zipPromise = JSZip.loadAsync(value).then(function(content) {
                var fileName = Object.keys(content.files)[0]
                console.log("Zipped DB file received, started extracting...");
                console.log("Name of the DB file inside Zip: " + fileName);
                return content.file(fileName).async("string");
            }).then(function(data) {
                console.log("DB data extracted, started parsing...");
                db = JSON.parse(data)
            })
        })
    } else {
        if(isJsonDB) {
            dbPromise = $.ajax({
                url: "data/data.json",
                dataType: "json"
            }).then(function(value) {
                db = value
            })
        } else {
            dbPromise = $.ajax({
                url: "data/data.xml",
                dataType: "text"
            }).then(xmlRes => {
                var options = {
                    attrPrefix : "",
                    textNodeName : "#text",
                    ignoreNonTextNodeAttr : false,
                    ignoreTextNodeAttr : false,
                    ignoreNameSpace : true,
                    ignoreRootElement : false,
                    textNodeConversion : true,
                    textAttrConversion : false,
                    arrayMode : false
                };
                db = Object.values(XmlParser.parse(xmlRes, options))[0]
            })
        }
    }

    xsdPromise = $.ajax({
        url: "data/mar.xsd",
        dataType: "text"
    }).then(xsdRes => {
        xsdParsePromise = convertString(xsdRes).then(function(value) {
            xsd = value
        })
    })

    if(isJsonDB) {
        parseDatabase = Xml.ParseJsonDatabase
    } else {
        parseDatabase = Xml.ParseXmlDatabase
    }
    
    return Promise.all([dbPromise]).then(function() {
        return Promise.all([zipPromise]).then(function() {
            return Promise.all([xsdPromise]).then(function() {
                return Promise.all([xsdParsePromise]).then(function() {
                    var res = parseDatabase(xsd, db)
                    console.log('MAR database is processed. Total processing time: ' + (performance.now() - start));
                    return res
                })
            })
        })
    })
    
}

/*$.ajaxTransport("+binary", function(options, originalOptions, jqXHR){
    // check for conditions and support for blob / arraybuffer response type
    if (window.FormData && ((options.dataType && (options.dataType == 'binary')) 
        || (options.data 
        && ((window.ArrayBuffer && options.data instanceof ArrayBuffer) 
        || (window.Blob && options.data instanceof Blob))))
       )
    {
        return {
            // create new XMLHttpRequest
            send: function(headers, callback){
        // setup all variables
                var xhr = new XMLHttpRequest(),
        url = options.url,
        type = options.type,
        async = options.async || true,
        // blob or arraybuffer. Default is blob
        dataType = options.responseType || "blob",
        data = options.data || null,
        username = options.username || null,
        password = options.password || null;

                xhr.addEventListener('load', function(){
            var data = {};
            data[options.dataType] = xhr.response;
            // make callback and send data
            callback(xhr.status
                    , xhr.statusText
                    , data
                    , xhr.getAllResponseHeaders());
                });

                xhr.open(type, url, async, username, password);

        // setup custom headers
        for (var i in headers ) {
            xhr.setRequestHeader(i, headers[i] );
        }

                xhr.responseType = dataType;
                xhr.send(data);
            },
            abort: function(){
                jqXHR.abort();
            }
        };
    }
});*/
