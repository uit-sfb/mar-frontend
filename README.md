## Getting started

### Quick start

Command to build `bundle.js`:
```
 npx webpack --mode development
```  

Command to run the example `index.html` with Caddy: 
```
caddy run --config ./caddy.json
```

The code UI should then be available at `localhost:3000`.
Note that you need to click on the `Browse` button to see the table.

### Using this library

- Add `@uit-sfb:registry=https://gitlab.com/api/v4/packages/npm` to your `.npmrc` project file.
- Add `"@uit-sfb/mar-frontend": "^1.0.0"` to the `dependencies` in your `package.json` project file.
- Run `npm install`

## Tech overview

This project defines some [React](https://reactjs.org/) components used by the MAR and Sars-CoV-2 databases UIs.  
[Webpack](https://webpack.js.org/) is used to bundle all the Javascript code into one `bundle.js`.  
[TypeScript](https://www.typescriptlang.org/) is a superset of Javascript supporting strong typing. It compiles directly to Javascript (see [webpack integration](https://webpack.js.org/guides/typescript/)).  
[DefinitelyTyped](https://github.com/DefinitelyTyped/DefinitelyTyped) is a library of TypeScript type definitions.  

### Testing

[Karma](https://karma-runner.github.io/latest/index.html) is used for testing (see [webpack integration](https://webpack.js.org/guides/integrations/)).  
Mocha, Sinon and Chai are 3 JS libraries for testing. See [here](https://rollout.io/blog/mocha-js-chai-sinon-frontend-javascript-code-testing-tutorial/).  
Putting them all [together](https://medium.com/@waelkdouh/unit-testing-using-typescript-mocha-chai-sinon-and-karma-de1fddafd42a).  

### Useful resources

- [Intro to React](https://www.reactenlightenment.com/what-is-react.html)
- [Getting started with Webpack](https://webpack.js.org/guides/getting-started/)





## Publishing

Before publishing a package to the gitlab npm registry, it is important to check if no other package with the same name exists within a given scope.  
Otherwise, package version should be updated in `package.json`.



