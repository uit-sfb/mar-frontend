import * as S from "../../src/utils/Statistics";
import * as M from "../../src/model/Model";

let testDatabase: M.MarDatabase = {
    records: [
        {
            mmpID: "123",
            attributesByName: {},
            attributes: [
                {
                    name: "investigationType",
                    type: M.AttributeType.String,
                    data: {
                        value: "Bacteria"
                    }
                },
                {
                    name: "investigationType",
                    type: M.AttributeType.String,
                    data: {
                        value: "Bacteria"
                    }
                },
                {
                    name: "investigationType",
                    type: M.AttributeType.String,
                    data: {
                        value: "Archaea"
                    }
                },
                {
                    name: "investigationType",
                    type: M.AttributeType.Missing,
                    data: {
                        reason: "missing"
                    }
                }
            ]
        }
    ],
    defaultAttributesOrder: [],
    type: "marref",
    recordsById: {}
}

describe("Database statistics", () => {
    it("should return correct investigationType statistics", () => {
        let stats = S.DatabaseStatistics(testDatabase)
        chai.assert(stats.investigationType["Bacteria"] === 2)
        chai.assert(stats.investigationType["Archaea"] === 1)
    })
})