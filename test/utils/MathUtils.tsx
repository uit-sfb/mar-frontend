import { NumberRange } from "../../src/utils/MathUtils";

describe("Range", () => {
    it("should return false if two ranges don't overlap", () => {
        let a = new NumberRange(1,2)
        let b = new NumberRange(3,4)
        chai.assert(!a.overlaps(b));
        chai.assert(!b.overlaps(a));        
    })

    it("should return true if two ranges overlap", () => {
        let a = new NumberRange(1,3)
        let b = new NumberRange(2,4)
        chai.assert(a.overlaps(b));
        chai.assert(b.overlaps(a));
    })

    it("should return true if one range is contained within another range", () => {
        let a = new NumberRange(1,5)
        let b = new NumberRange(2,4)
        let c = new NumberRange(1,3);
        chai.assert(a.overlaps(b));
        chai.assert(b.overlaps(a));
    })

    it("should return true if the boudries overlap", () => {
        let a = new NumberRange(1,3)
        let b = new NumberRange(3,4)
        chai.assert(a.overlaps(b));
        chai.assert(b.overlaps(a));
    })
})