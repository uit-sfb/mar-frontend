import * as Model from "../../src/model/Model";
import * as F from "../../src/utils/Filters";
import { NumberRange } from "../../src/utils/MathUtils";

let T = Model.AttributeType

let MockFilter: F.AttributeFilter<boolean> = (arg: F.AttributeFilterArgs<boolean>) => {
    return arg.params;
}

let missingAttr: Model.Attribute = {
    type: T.Missing,
    name: "",
    data: {
        reason: ""
    }
}

describe("Filters", () => {
    it("should have a nice API when seen from the DatabasePage", () => {
        let records: Array<Model.MarRecord> = []

        // This is given by the ReactRouter component
        let query: {[key:string]:string} = {
            "f_env_package": "Air",
            "f_env_biome": "hello"
        }

        // This should happen in the DatabasePage component
        let filters: Array<F.RecordFilter> = [
            F.RecordFilter(F.StringFilter, {equals: query["f_env_package"]}, "envPackage"),
            F.RecordFilter(F.StringFilter, {equals: query["f_env_biome"]}, "envBiome")
        ];

        let filteredRecords = F.FilterRecords(filters, records);

        // now we can render the filtered records
    })

    it("should have a nice API when seen from a filter view component", () => {

        // These should be given as props

        function callback(p: F.StringFilterParams) {
            // do nothing
        }

        let filteredDatabase: Array<Model.MarRecord> = []

        let params: F.StringFilterParams = {
            equals: "hello"
        }

        let attrName = "envPackage"


        // This should happen in the component 'render' function

        let filters: Array<F.RecordFilter> = [
            F.RecordFilter(F.StringFilter, params, attrName),
        ];

        let filteredRecords = F.FilterRecords(filters, filteredDatabase);

        // filteredRecords can now be used to render the component

        // This should happen when the user updates the view
        callback({
            equals: "new search value"
        })
    })

    describe("RecordFilter", () => {
        it("should return the correct number of results", () => {
            let testRecord: Model.MarRecord = {
                attributes: [],
                attributesByName: {},
                mmpID: ""
            };
            let records: Array<Model.MarRecord> = [
                testRecord, testRecord, testRecord, testRecord
            ]

            let recordFilter = F.RecordFilter(MockFilter, true, "");
            let filteredRecords = recordFilter(records);

            let length = filteredRecords.length;
            console.log(length);
            chai.assert(length === records.length);
        })
    })

    describe("TextFilter", () => {
        it("should match any attribute that matches the query text", () => {
            let matchAttr: Model.Attribute = {
                type: Model.AttributeType.String,
                name: "hello",
                data: {
                    value: "hola"
                }
            }

            let noMatchAttr: Model.Attribute = {
                type: Model.AttributeType.String,
                name: "hello",
                data: {
                    value: "hello"
                }
            }

            let matchRecord: Model.MarRecord = {
                attributes: [
                    matchAttr
                ],
                attributesByName: {},
                mmpID: "match"
            };

            let noMatchRecord: Model.MarRecord = {
                attributes: [
                    noMatchAttr
                ],
                attributesByName: {},
                mmpID: "noMatch"
            };

            let records: Array<Model.MarRecord> = [noMatchRecord, matchRecord];

            let res = F.TextFilter({query: "hola"})(records);
            chai.assert(res.length === 1);
            chai.assert(res[0].mmpID === "match");
        });
    });

    describe("StringFilter", () => {
        let attr: Model.Attribute = {
            type: T.String,
            data: {
                value: "hello"
            },
            name: ""
        }

        it("should remove any record that does not match the 'equals' parameter", () => {
            let res = F.StringFilter({
                attribute: attr,
                params: {
                    equals: "hello-world"
                }
            })

            chai.assert(res === false);
        })

        it("should not remove any record that matches the 'equals' parameter", () => {
            let res = F.StringFilter({
                attribute: attr,
                params: {
                    equals: "hello"
                }
            })

            chai.assert(res === true);
        })

        it("should remove any attributes that are 'missing'", () => {
            let res = F.StringFilter({
                attribute: {
                    type: T.Missing,
                    data: {
                        reason: "lol"
                    },
                    name: "wtf"
                },
                params: {
                    equals: "hello"
                }
            })

            chai.assert(res === false);
        })
    })

    describe("PrecisionDecimalStringFilter", () => {
        let trueFixed = new NumberRange(2,2);
        let falseFixed = new NumberRange(5,5);

        let trueRange = new NumberRange(1,5);
        let falseRange = new NumberRange(5,6);

        let rangeAttr: Model.Attribute = {
            name: "",
            type: T.PrecisionDecimalStringRange,
            data: {
                min: "1",
                max: "3"
            }
        }

        let fixedAttr: Model.Attribute = {
            name: "",
            type: T.PrecisionDecimalStringFixed,
            data: {
                value: "2"
            }
        }

        let filter = F.PrecicionDecimalStringFilter

        it("should work with query range -> PrecisionDecimalStringRange", () => {
            chai.assert(filter({
                attribute: rangeAttr,
                params: trueRange
            }));

            chai.assert(!filter({
                attribute: rangeAttr,
                params: falseRange
            }));
        })

        it("should work with query range -> PrecisionDecimalStringFixed", () => {
            chai.assert(filter({
                attribute: fixedAttr,
                params: trueRange
            }));

            chai.assert(!filter({
                attribute: fixedAttr,
                params: falseRange
            }));
        })

        it("should work with fixed query -> PrecisionDecimalStringRange", () => {
            chai.assert(filter({
                attribute: rangeAttr,
                params: trueFixed
            }));

            chai.assert(!filter({
                attribute: rangeAttr,
                params: falseFixed
            }));
        })

        it("should work with fixed query -> PrecisionDecimalStringFixed", () => {
            chai.assert(filter({
                attribute: fixedAttr,
                params: trueFixed
            }));

            chai.assert(!filter({
                attribute: fixedAttr,
                params: falseFixed
            }));
        })

        it("should return false when attribute is missing", () => {
            chai.assert(!filter({
                attribute: missingAttr,
                params: trueFixed
            }));
        })
        
        it("should find the value 200", () => {
            chai.assert(filter({
                attribute: {
                    name: "",
                    type: T.PrecisionDecimalStringFixed,
                    data: {
                        value: "200"
                    }
                },
                params: new NumberRange(200, 200)
            }));
        })
    })

    describe("YearFilter", () => {
        let trueFixed = new NumberRange(2009, 2009);
        let falseFixed = new NumberRange(2016, 2016);

        let trueRange = new NumberRange(2008, 2010);
        let falseRange = new NumberRange(2016, 2017);

        let rangeAttr: Model.Attribute = {
            name: "",
            type: T.PrecisionDateTimeRange,
            data: {
                from: {
                    year: 2008
                },
                to: {
                    year: 2010
                }
            }
        }

        let fixedAttr: Model.Attribute = {
            name: "",
            type: T.PrecisionDateTimeFixed,
            data: {
                year: 2009
            }
        }

        let filter = F.YearFilter;

        it("should work with query range -> PrecisionDateTimeRange", () => {
            chai.assert(filter({
                attribute: rangeAttr,
                params: trueRange
            }));

            chai.assert(!filter({
                attribute: rangeAttr,
                params: falseRange
            }));
        })

        it("should work with query range -> PrecisionDateTimeFixed", () => {
            chai.assert(filter({
                attribute: fixedAttr,
                params: trueRange
            }));

            chai.assert(!filter({
                attribute: fixedAttr,
                params: falseRange
            }));
        })

        it("should work with fixed query -> PrecisionDateTimeRange", () => {
            chai.assert(filter({
                attribute: rangeAttr,
                params: trueFixed
            }));

            chai.assert(!filter({
                attribute: rangeAttr,
                params: falseFixed
            }));
        })

        it("should work with fixed query -> PrecisionDateTimeFixed", () => {
            chai.assert(filter({
                attribute: fixedAttr,
                params: trueFixed
            }));

            chai.assert(!filter({
                attribute: fixedAttr,
                params: falseFixed
            }));
        })

        it("should return false when attribute is missing", () => {
            chai.assert(!filter({
                attribute: missingAttr,
                params: trueFixed
            }));
        })
    })
})

describe("Attribute Mappers", () => {
    describe("FilterRange", () => {
        describe("Mappers", () => {
            describe("YearMapper", () => {
                let YearMapper = F.YearRangeMapper;
                let rangeAttr: Model.Attribute = {
                    name: "",
                    type: T.PrecisionDateTimeRange,
                    data: {
                        from: {
                            year: 2008
                        },
                        to: {
                            year: 2010
                        }
                    }
                }

                let fixedAttr: Model.Attribute = {
                    name: "",
                    type: T.PrecisionDateTimeFixed,
                    data: {
                        year: 2009
                    }
                }

                it("should map a PrecisionDateTimeFixed to a RangeModel", () => {
                    let mapped = YearMapper(fixedAttr);
                    chai.assert(mapped.from.value === 2009)
                    chai.assert(mapped.from.display === "2009")
                    chai.assert(mapped.to.value === 2009)
                    chai.assert(mapped.to.display === "2009")
                })

                it("should map a PrecisionDateTimeRange to a RangeModel", () => {
                    let mapped = YearMapper(rangeAttr);
                    chai.assert(mapped.from.value === 2008)
                    chai.assert(mapped.from.display === "2008")
                    chai.assert(mapped.to.value === 2010)
                    chai.assert(mapped.to.display === "2010")
                })
            })

            describe("DecimalUnitRangeMapper", () => {
                let rangeAttr: Model.Attribute = {
                    name: "",
                    type: T.PrecisionDecimalStringRange,
                    data: {
                        min: "10.5",
                        max: "20"
                    }
                }

                let fixedAttr: Model.Attribute = {
                    name: "",
                    type: T.PrecisionDecimalStringFixed,
                    data: {
                        value: "20"
                    }
                }

                let mapper = F.DecimalUnitRangeMapper("m");

                it("should map a PrecisionDecimalStringRange to RangeModel", () => {
                    let mapped = mapper(rangeAttr)
                    chai.assert.strictEqual(mapped.from.value, 10.5);
                    chai.assert.strictEqual(mapped.from.display, "10.5 m");
                    chai.assert.strictEqual(mapped.to.value, 20);
                    chai.assert.strictEqual(mapped.to.display, "20 m");
                })

                it("should map a PrecisionDecimalStringFixed to RangeModel", () => {
                    let mapped = mapper(fixedAttr)
                    chai.assert.strictEqual(mapped.from.value, 20)
                    chai.assert.strictEqual(mapped.from.display, "20 m")
                    chai.assert.strictEqual(mapped.to.value, 20)
                    chai.assert.strictEqual(mapped.to.display, "20 m")
                })
            })
        })
    })
});