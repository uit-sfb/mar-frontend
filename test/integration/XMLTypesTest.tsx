import * as React from "react";
import * as ReactDOM from "react-dom";
import * as Xml from "../../src/integration/Xml"
import { Attribute, AttributeType, Missing, String } from "../../src/model/Model"
import * as $ from 'jquery';
import * as Model from "../../src/model/Model"

function parse(xml: string): Element {
    let parser = new DOMParser();
    let doc = parser.parseFromString(xml, "text/xml")
    return doc.firstChild as Element;
}

let testSchema = 
    "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
           "targetNamespace=\"http://sfb.uit.no/schemas/mar\" " +
           "xmlns=\"http://sfb.uit.no/schemas/mar\" elementFormDefault=\"qualified\">" +
        "<xs:element name=\"records\" type=\"MarRecords\"/>" +
        "<xs:complexType name=\"MarRecords\">"+
            "<xs:sequence>"+
                "<xs:element name=\"record\" type=\"MarRecord\" maxOccurs=\"unbounded\" minOccurs=\"0\"/>"+
            "</xs:sequence>"+
        "</xs:complexType>"+
        "<xs:complexType name=\"MarRecord\">"+
            "<xs:sequence>"+
                "<xs:choice minOccurs=\"0\">"+
                    "<xs:element name=\"altElev\" type=\"PrecisionDecimalString\"/>"+
                    "<xs:element name=\"altElevMissing\" type=\"Missing\"/>"+
                "</xs:choice>"+
                "<xs:choice minOccurs=\"0\">"+
                    "<xs:element name=\"collectionDate\" type=\"PrecisionDateTime\"/>"+
                    "<xs:element name=\"collectionDateMissing\" type=\"Missing\"/>"+
                "</xs:choice>"+
                "<xs:choice minOccurs=\"0\">"+
                    "<xs:element name=\"envBiome\" type=\"xs:string\"/>"+
                    "<xs:element name=\"envBiomeMissing\" type=\"Missing\"/>"+
                "</xs:choice>"+
                "<xs:choice minOccurs=\"0\">"+
                    "<xs:element name=\"investigationType\" type=\"InvestigationType\" minOccurs=\"0\"/>"+
                    "<xs:element name=\"investigationTypeMissing\" type=\"Missing\"/>"+
                "</xs:choice>"+
                "<xs:choice minOccurs=\"0\">"+
                    "<xs:element name=\"hostSex\" type=\"HostSex\"/>"+
                    "<xs:element name=\"hostSexMissing\" type=\"Missing\"/>"+
                "</xs:choice>"+
                "<xs:choice minOccurs=\"0\">"+
                    "<xs:element name=\"pathovar\" type=\"xs:string\"/>"+
                    "<xs:element name=\"pathovarMissing\" type=\"Missing\"/>"+
                "</xs:choice>"+
            "</xs:sequence> "+
        "</xs:complexType>"+
    "</xs:schema>"

// WARNING: THIS IS NOT UP TO DATE WITH THE LATEST SCHEMA!!!!
let testRecord = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"+
"<records xmlns=\"http://sfb.uit.no/schemas/mar\">"+
    "<record>"+
        "<altElevMissing>"+
            "<reason>missing</reason>"+
        "</altElevMissing>"+
        "<collectionDate xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\" dateTime=\"2006-01-01T00:00:00Z\"/>"+
        "<depth xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDecimalStringRange\">"+
            "<min>0.08</min>"+
            "<max>0.12</max>"+
        "</depth>"+
        "<envBiome>marine hydrothermal vent</envBiome>"+
        "<envBiomeEnvo>ENVO:00002113</envBiomeEnvo>"+
        "<envFeatureMissing>"+
            "<reason>notAvailable</reason>"+
        "</envFeatureMissing>"+
        "<envFeatureEnvoMissing>"+
            "<reason>missing</reason>"+
        "</envFeatureEnvoMissing>"+
        "<envMaterial>rhizosphere</envMaterial>"+
        "<envMaterialEnvo>ENVO:00002150</envMaterialEnvo>"+
        "<envPackage>Host-associated</envPackage>"+
        "<envSalinity>17.86</envSalinity>"+
        "<envTemp>15.8</envTemp>"+
        "<geoLocName>"+
            "<item>ABE hydrothermal field (20° 45.8' S 176° 11.5' W) on the Eastern Lau Spreading Centre at a depth of 2104-2163 m</item>"+
        "</geoLocName>"+
        "<geoLocNameGaz>Iriomote-Ishigaki National Park</geoLocNameGaz>"+
        "<geoLocNameGazEnvo>GAZ:00006974</geoLocNameGazEnvo>"+
        "<investigationTypeMissing>"+
            "<reason>missing</reason>"+
        "</investigationTypeMissing>"+
        "<latLon>"+
            "<latitude>-2.343556</latitude>"+
            "<longitude>36.045844</longitude>"+
        "</latLon>"+
        "<pathogenicity>Fish</pathogenicity>"+
        "<projectName>Draft genome sequence of Nocardia seriolae</projectName>"+
        "<assembly>Velvet v. 1.2.05</assembly>"+
        "<isolGrowthCondt>"+
            "<accession>10.1007/978-3-642-30141-4_75:2112896:25527835</accession>"+
        "</isolGrowthCondt>"+
        "<numReplicons>2</numReplicons>"+
        "<refBiomaterial>"+
            "<accession>10.1007/978-3-642-30141-4_75:2112896:25527835</accession>"+
        "</refBiomaterial>"+
        "<microbePackage>MIGS: cultured bacteria/archaea:version 4.0</microbePackage>"+
        "<sampleType>Monoisolate</sampleType>"+
        "<strain>0714S6-1:MCCC 1A05965</strain>"+
        "<isolationSource>seawater enriched with propanol</isolationSource>"+
        "<collectedBy>Midori Kurahashi:Akira Yokota</collectedBy>"+
        "<cultureCollection>"+
            "<item>DSM 19584</item>"+
            "<item>ATCC 25917</item>"+
            "<item>CCUG 28585</item>"+
            "<item>CECT 595</item>"+
            "<item>CIP 75.05</item>"+
            "<item>LMG 3895</item>"+
            "<item>NCMB 1897</item>"+
        "</cultureCollection>"+
        "<kingdom>Archaea</kingdom>"+
        "<phylum>delta/epsilon subdivisions</phylum>"+
        "<class>Gammaproteobacteria</class>"+
        "<order>Pseudomonadales</order>"+
        "<family>Clostridiales Family XVII. Incertae Sedis</family>"+
        "<genus>Candidatus Puniceispirillum</genus>"+
        "<species>Aeromonas salmonicida subsp. salmonicida</species>"+
        "<taxonIdentifier>1051632</taxonIdentifier>"+
        "<taxonLinageIds>"+
            "<accession>131567</accession>"+
            "<accession>2</accession>"+
            "<accession>1224</accession>"+
            "<accession>28211</accession>"+
            "<accession>82117</accession>"+
            "<accession>62654</accession>"+
            "<accession>767891</accession>"+
            "<accession>767892</accession>"+
            "<accession>488538</accession>"+
        "</taxonLinageIds>"+
        "<taxonLinageNames>"+
            "<item>cellular organisms</item>"+
            "<item>Bacteria</item>"+
            "<item>Proteobacteria</item>"+
            "<item>Gammaproteobacteria</item>"+
            "<item>Pseudomonadales</item>"+
            "<item>Pseudomonadaceae</item>"+
            "<item>Pseudomonas</item>"+
            "<item>Pseudomonas stutzeri group</item>"+
            "<item>Pseudomonas stutzeri subgroup</item>"+
            "<item>Pseudomonas stutzeri</item>"+
            "<item>Pseudomonas stutzeri CCUG 29243</item>"+
        "</taxonLinageNames>"+
        "<backdiveId>13801</backdiveId>"+
        "<curationDate>"+
            "<date>2016-11-01</date>"+
        "</curationDate>"+
        "<implementationDate>"+
            "<date>2016-05-26</date>"+
        "</implementationDate>"+
        "<mmpBiome>Marine</mmpBiome>"+
        "<mmpID>1</mmpID>"+
        "<silvaAccessionSSU>721</silvaAccessionSSU>"+
        "<silvaAccessionLSU>721</silvaAccessionLSU>"+
        "<uniprotProteomeIDMissing>"+
            "<reason>notAvailable</reason>"+
        "</uniprotProteomeIDMissing>"+
        "<assemblyAccessionGenbank>GCA_000400245.1</assemblyAccessionGenbank>"+
        "<bioprojectAccession>PRJDB721</bioprojectAccession>"+
        "<biosampleAccession>SAMD00036654</biosampleAccession>"+
        "<genbankAccession>BAOA00000000</genbankAccession>"+
        "<NcbiRefseqAccession>NZ_AAOX00000000</NcbiRefseqAccession>"+
        "<NcbiTaxonIdentifier>1280001</NcbiTaxonIdentifier>"+
        "<annotationProvider>NCBI</annotationProvider>"+
        "<annotationDateMissing>"+
            "<reason>missing</reason>"+
        "</annotationDateMissing>"+
        "<annotationPipeline>NCBI Prokaryotic Genome Annotation Pipeline</annotationPipeline>"+
        "<annotationMethod>"+
            "<item>Best-placed reference protein set</item>"+
            "<item>GeneMarkS+</item>"+
        "</annotationMethod>"+
        "<annotationSoftwareRevision xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDecimalStringFixed\">"+
            "<value>3</value>"+
        "</annotationSoftwareRevision>"+
        "<featuresAnnotatedMissing>"+
            "<reason>missing</reason>"+
        "</featuresAnnotatedMissing>"+
        "<genes>5429</genes>"+
        "<cds>5159</cds>"+
        "<pseudoGenesMissing>"+
            "<reason>missing</reason>"+
        "</pseudoGenesMissing>"+
        "<rrnas>"+
            "<rrna5S>5</rrna5S>"+
            "<rrna16S>10</rrna16S>"+
            "<rrna23S>10</rrna23S>"+
        "</rrnas>"+
        "<completeRrnas>"+
            "<rrna5S>2</rrna5S>"+
            "<rrna16S>0</rrna16S>"+
            "<rrna23S>0</rrna23S>"+
        "</completeRrnas>"+
        "<partialRrnasMissing>"+
            "<reason>missing</reason>"+
        "</partialRrnasMissing>"+
        "<trnas>112</trnas>"+
        "<ncrna>1</ncrna>"+
        "<frameshiftedGenesMissing>"+
            "<reason>missing</reason>"+
        "</frameshiftedGenesMissing>"+
        "<frameshiftedGenesOnMonomerRunsMissing>"+
            "<reason>missing</reason>"+
        "</frameshiftedGenesOnMonomerRunsMissing>"+
        "<frameshiftedGenesNotOnMonomerRunsMissing>"+
            "<reason>missing</reason>"+
        "</frameshiftedGenesNotOnMonomerRunsMissing>"+
        "<hostCommonName>Asian seabass</hostCommonName>"+
        "<hostScientificName>Lates calcarifer</hostScientificName>"+
        "<organism>Vibrio jasicida</organism>"+
        "<fullScientificName>Vibrio jasicida MWB 21</fullScientificName>"+
        "<disease>Nocardiosis</disease>"+
        "<publicationPmid>"+
            "<accession>22066815:23277585:24158624</accession>"+
        "</publicationPmid>"+
        "<isolationCountry>Netherlands</isolationCountry>"+
        "<isolationComments>Isolated by Martinus W. Beijerinck in 1920s and stored at the Netherland Culture Collection of Bacteria (NCCB)</isolationComments>"+
        "<comments>Genome sequence of Vibrio jasicida MWB 21. The strain was was part of a collection of bacteria isolated by Martinus W. Beijerinck in 1920s and stored at the Netherland Culture Collection of Bacteria (NCCB). The sequencing was done as a part of a study analyzing closely related bacteria in the so-called Harveyi clade (genus Vibrio, family Vibrionaceae).</comments>"+
        "<sequencingCenters>University of Miyazaki</sequencingCenters>"+
        "<bodySampleSite>gill</bodySampleSite>"+
        "<bodySampleSubSiteMissing>"+
            "<reason>missing</reason>"+
        "</bodySampleSubSiteMissing>"+
        "<otherClinical>host_disease_outcome:no disease, host_disease_stage:no disease, host_health_state:no disease</otherClinical>"+
        "<gramStain>Positive</gramStain>"+
        "<cellShape>CurvedShaped</cellShape>"+
        "<motility>false</motility>"+
        "<sporulation>true</sporulation>"+
        "<temperatureRangeMissing>"+
            "<reason>missing</reason>"+
        "</temperatureRangeMissing>"+
        "<optimalTemperatureMissing>"+
            "<reason>missing</reason>"+
        "</optimalTemperatureMissing>"+
        "<halotolerance>ModerateHalophilic</halotolerance>"+
        "<oxygenRequirementMissing>"+
            "<reason>missing</reason>"+
        "</oxygenRequirementMissing>"+
        "<plasmidsMissing>"+
            "<reason>missing</reason>"+
        "</plasmidsMissing>"+
        "<genomeLength>5927294</genomeLength>"+
        "<gcContent>45.2</gcContent>"+
        "<refseqCds>0</refseqCds>"+
        "<biovar>biovar AF</biovar>"+
        "<otherTypingMissing>"+
            "<reason>missing</reason>"+
        "</otherTypingMissing>"+
        "<typeStrain>true</typeStrain>"+
        "<sequencingPlatform>"+
            "<item>454 GS FLX+:Ion Torrent PGM</item>"+
        "</sequencingPlatform>"+
        "<sequencingDepth>"+
            "<item>16.70</item>"+
            "<item>3.74</item>"+
        "</sequencingDepth>"+
        "<contigs>171</contigs>"+
        "<genomeStatus>Draft</genomeStatus>"+
        "<hostSex>Male</hostSex>"+
        "<hostHealthStageMissing>"+
            "<reason>missing</reason>"+
        "</hostHealthStageMissing>"+
        "<hostAge>"+
            "<amount xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDecimalStringFixed\">"+
                "<value>6</value>"+
            "</amount>"+
            "<unit>days</unit>"+
        "</hostAge>"+
        "<serovar>O2: K28</serovar>"+
        "<pathovarMissing>"+
            "<reason>missing</reason>"+
        "</pathovarMissing>"+
    "</record>"+
"</records>"

let testStringAttribute: Attribute = {
    name: "hostSex",
    type: AttributeType.String,
    data: {
        value: "Male"
    }
}

let testMissingAttribute: Attribute = {
    name: "pathovar",
    type: AttributeType.Missing,
    data: {
        reason: "missing"
    }
}

describe("XSD Parser", () => {
    it("should be able to find all the element types", () => {
        let parser = new DOMParser();
        let doc = parser.parseFromString(testSchema, "text/xml")        
        let parsed = Xml.parseXSD(doc)
        chai.assert(parsed.elementTypes["altElev"] === "PrecisionDecimalString")
    })
})

describe("XML Parser", () => {
    it("should parse a mar record", () => {
        let parser = new DOMParser();
        let schema = parser.parseFromString(testSchema, "text/xml");
        let database = parser.parseFromString(testRecord, "text/xml");
        let db = Xml.ParseDatabase(schema, database);
        chai.assert(db.records.length === 1);
        let record = db.records[0];

        let hostSex: Attribute
        let pathovar: Attribute
        for(let attr of record.attributes) {
            console.log(attr)
            if(attr.name === "pathovar") {
                pathovar = attr;
            }
            if(attr.name === "hostSex") {
                hostSex = attr;
            }
        }
        
        chai.assert(hostSex.type === AttributeType.String)
        chai.assert((hostSex.data as String).value === "Male")
        chai.assert(pathovar.type === AttributeType.Missing)
        chai.assert((pathovar.data as Missing).reason === "missing")
    })
})

describe("Attribute parsers", () => {
    it("should parse the Missing type (XMissing)", () => {
        let element = parse("<helloMissing><reason>missing</reason></helloMissing>");
        let xm = Xml.XMissing("test", element);
        chai.assert(xm.type === AttributeType.Missing)
        let attribute = xm.data as Missing
        chai.assert(attribute.reason === "missing")
    })

    it("should parse the xs:boolean type (XBoolean)", () => {
        let element = parse("<hello>false</hello>");
        let xm = Xml.XBoolean("hello", element);
        chai.assert(xm.type === AttributeType.Boolean)
        let attribute = xm.data as Boolean
        chai.assert(attribute === false)
    })

    it("should parse the xs:string type (XString)", () => {
        let xml = parse("<hello>world</hello>");
        let xs = Xml.XString("test", xml);
        chai.assert(xs.type === AttributeType.String)
        let attribute = xs.data as String
        chai.assert(attribute.value === "world");
    })
    it("should parse the xs:string type (XString)", () => {
        let xml = parse("<hello></hello>");
        let xs = Xml.XString("test", xml);
        chai.assert(xs.type === AttributeType.String)
        let attribute = xs.data as String
        chai.assert(attribute.value === "");
    })

    it("should parse the PrecisionDecimalStringFixed type (XPrecisionDecimalStringFixed)", () => {
        let xml = parse("<annotationSoftwareRevision xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDecimalStringFixed\"><value>3</value></annotationSoftwareRevision>");
        let xfl = Xml.XPrecisionDecimalStringFixed("test", xml);
        chai.assert(xfl.type === AttributeType.PrecisionDecimalStringFixed)
        let attribute = xfl.data as String
        chai.assert(attribute.value === "3");
    })

    it("should parse the PrecisionDecimalStringRange type (XPrecisionDecimalStringRange)", () => {
        let xml = parse("<optimalTemperature xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDecimalStringRange\"><min>28.00</min><max>32.00</max></optimalTemperature>");
        let xfl = Xml.XPrecisionDecimalStringRange("test", xml);
        chai.assert(xfl.type === AttributeType.PrecisionDecimalStringRange)
        let attribute = xfl.data as Model.PrecisionDecimalStringRange
        chai.assert(attribute.min === "28.00");
        chai.assert(attribute.max === "32.00");
    })

    it("should parse the Duration type Range (XDuration)", () => {
        let xml = parse("<hostAge><amount xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDecimalStringRange\"><min>2.5</min><max>3.0</max></amount><unit>hours</unit></hostAge>");
        let xfl = Xml.XDuration("test", xml);
        chai.assert(xfl.type === AttributeType.Duration)
        let attribute = xfl.data as Model.Duration
        chai.assert(attribute.unit === "hours");
        chai.assert((attribute.amount as Model.PrecisionDecimalStringRange).min === "2.5");
        chai.assert((attribute.amount as Model.PrecisionDecimalStringRange).max === "3.0");
    })

    it("should parse the Duration type Fixed (XDuration)", () => {
        let xml = parse("<hostAge><amount xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDecimalStringFixed\"><value>10</value></amount><unit>years</unit></hostAge>");
        let xfl = Xml.XDuration("test", xml);
        chai.assert(xfl.type === AttributeType.Duration)
        let attribute = xfl.data as Model.Duration
        chai.assert(attribute.unit === "years");
        chai.assert((attribute.amount as String).value === "10");
    })

    it("should parse the PrecisionDateTimeFixed/year type (XPrecisionDateTimeFixed)", () => {
        let xml = parse("<collectionDate xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><year>2000</year></collectionDate>");
        let xdtf = Xml.XPrecisionDateTimeFixed("test", xml);
        chai.assert(xdtf.type === AttributeType.PrecisionDateTimeFixed)
        let attribute = xdtf.data as Model.PrecisionDateTimeFixed
        chai.assert(attribute.year === 2000);
    })

    it("should parse the PrecisionDateTimeFixed/yearMonth type (XPrecisionDateTimeFixed)", () => {
        let xml = parse("<collectionDate xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><yearMonth>2016-12</yearMonth></collectionDate>");
        let xdtf = Xml.XPrecisionDateTimeFixed("test", xml);
        chai.assert(xdtf.type === AttributeType.PrecisionDateTimeFixed)
        let attribute = xdtf.data as Model.PrecisionDateTimeFixed
        chai.assert(attribute.month === 12);
        chai.assert(attribute.year === 2016);
    })

    it("should parse the PrecisionDateTimeFixed/date type (XPrecisionDateTimeFixed)", () => {
        let xml = parse("<collectionDate xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><date>2016-05-26</date></collectionDate>");
        let xdtf = Xml.XPrecisionDateTimeFixed("test", xml);
        chai.assert(xdtf.type === AttributeType.PrecisionDateTimeFixed)
        let attribute = xdtf.data as Model.PrecisionDateTimeFixed
        chai.assert(attribute.date === 26);
        chai.assert(attribute.month === 5);
        chai.assert(attribute.year === 2016);
    })

    it("should parse the PrecisionDateTimeFixed/dateTime type (XPrecisionDateTimeFixed)", () => {
        let xml = parse("<collectionDate xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><dateTime>1994-02-26T05:32:48Z</dateTime></collectionDate>");
        let xdtf = Xml.XPrecisionDateTimeFixed("test", xml);
        chai.assert(xdtf.type === AttributeType.PrecisionDateTimeFixed)
        let attribute = xdtf.data as Model.PrecisionDateTimeFixed
        chai.assert(attribute.milliseconds === 0);
        chai.assert(attribute.seconds === 48);
        chai.assert(attribute.minutes === 32);
        chai.assert(attribute.hours === 5);
        chai.assert(attribute.date === 26);
        chai.assert(attribute.month === 2);
        chai.assert(attribute.year === 1994);
    })

    it("should parse the PrecisionDateTimeRange/year type (XPrecisionDateTimeRange)", () => {
        let xml = parse("<collectionDate xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeRange\"><from xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><year>2000</year></from><to xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><year>2005</year></to><collectionDate/>");
        let xdtf = Xml.XPrecisionDateTimeRange("test", xml);
        chai.assert(xdtf.type === AttributeType.PrecisionDateTimeRange)
        let attribute = xdtf.data as Model.PrecisionDateTimeRange
        chai.assert(attribute.from.year === 2000);
        chai.assert(attribute.to.year === 2005);
    })

    it("should parse the PrecisionDateTimeRange/yearMonth type (XPrecisionDateTimeRange)", () => {
        let xml = parse("<collectionDate xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeRange\"><from xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><yearMonth>2012-07</yearMonth></from><to xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><yearMonth>2016-12</yearMonth></to><collectionDate/>");
        let xdtf = Xml.XPrecisionDateTimeRange("test", xml);
        chai.assert(xdtf.type === AttributeType.PrecisionDateTimeRange)
        let attribute = xdtf.data as Model.PrecisionDateTimeRange
        chai.assert(attribute.from.year === 2012);
        chai.assert(attribute.from.month === 7);
        chai.assert(attribute.to.year === 2016);
        chai.assert(attribute.to.month === 12);
    })

    it("should parse the PrecisionDateTimeRange/date type (XPrecisionDateTimeRange)", () => {
        let xml = parse("<collectionDate xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeRange\"><from xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><date>2012-07-26</date></from><to xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><date>2016-12-11</date></to><collectionDate/>");
        let xdtf = Xml.XPrecisionDateTimeRange("test", xml);
        chai.assert(xdtf.type === AttributeType.PrecisionDateTimeRange)
        let attribute = xdtf.data as Model.PrecisionDateTimeRange
        chai.assert(attribute.from.year === 2012);
        chai.assert(attribute.from.month === 7);
        chai.assert(attribute.from.date === 26);
        chai.assert(attribute.to.year === 2016);
        chai.assert(attribute.to.month === 12);
        chai.assert(attribute.to.date === 11);
    })

    it("should parse the PrecisionDateTimeRange/dateTime type (XPrecisionDateTimeRange)", () => {
        let xml = parse("<collectionDate xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeRange\"><from xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><dateTime>2012-07-26T11:32:48Z</dateTime></from><to xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"PrecisionDateTimeFixed\"><dateTime>2016-12-11T05:55:01Z</dateTime></to><collectionDate/>");
        let xdtf = Xml.XPrecisionDateTimeRange("test", xml);
        chai.assert(xdtf.type === AttributeType.PrecisionDateTimeRange)
        let attribute = xdtf.data as Model.PrecisionDateTimeRange
        chai.assert(attribute.from.year === 2012);
        chai.assert(attribute.from.month === 7);
        chai.assert(attribute.from.date === 26);
        chai.assert(attribute.from.hours === 11);
        chai.assert(attribute.from.minutes === 32);
        chai.assert(attribute.from.seconds === 48);
        chai.assert(attribute.from.milliseconds === 0);
        chai.assert(attribute.to.year === 2016);
        chai.assert(attribute.to.month === 12);
        chai.assert(attribute.to.date === 11);
        chai.assert(attribute.to.hours === 5);
        chai.assert(attribute.to.minutes === 55);
        chai.assert(attribute.to.seconds === 1);
        chai.assert(attribute.to.milliseconds === 0);
    })

    it("should parse the Accession type (XAccession)", () => {
        let xml = parse("<hello url=\"http://example.com/123\">123</hello>");
        let xa = Xml.XAccession("test", xml);
        chai.assert(xa.type === AttributeType.Accession);
        chai.assert(xa.name === "test");
        let attr = xa.data as Model.Accession;
        chai.assert(attr.url === "http://example.com/123");
        chai.assert(attr.value === "123");

        let xmlWithoutUrl = parse("<hello>123</hello>");
        let xWoUrl = Xml.XAccession("test", xmlWithoutUrl);
        let attrW = xWoUrl.data as Model.Accession;
        chai.assert(attrW.url === null);
        chai.assert(attrW.value === "123");
    })
    it("should parse the Rrnas type (XRrnas)", () => {
        let xml = parse("<rrnas><rrna5S>5</rrna5S><rrna23S>7</rrna23S><rrna16S>10</rrna16S></rrnas>");
        let xs = Xml.XRrnas("test", xml);
        chai.assert(xs.type === AttributeType.Rrnas)
        let attribute = xs.data as Model.Rrnas;
        chai.assert(attribute.rrna5S === 5);
        chai.assert(attribute.rrna16S === 10);
        chai.assert(attribute.rrna23S === 7);
    })
    it("should parse the OtherTyping type (XOtherTyping)", () => {
        let xml = parse("<othertyping><typing>genotype</typing><term>Wild Type</term></otherTyping>");
        let xs = Xml.XOtherTyping("test", xml);
        chai.assert(xs.type === AttributeType.OtherTyping)
        let attribute = xs.data as Model.OtherTyping;
        chai.assert(attribute.typing === "genotype");
        chai.assert(attribute.term === "Wild Type");
    })

    it("should parse the Position type (XPosition)", () => {
        let xml = parse(
            "<latLon>"+
                "<latitude>-2.343556</latitude>"+
                "<longitude>36.045844</longitude>"+
            "</latLon>");
        let xs = Xml.XPosition("test", xml);
        chai.assert(xs.type === AttributeType.Position)
        let attribute = xs.data as Model.Position;
        chai.assert(attribute.latitude === -2.343556);
        chai.assert(attribute.longitude === 36.045844);
    })
    it("should parse the StringList type (XStringList)", () => {
        let xml = parse("<taxonLinageNames><item>cellular organisms</item><item>Bacteria</item><item>Proteobacteria</item><item>Alphaproteobacteria</item><item>Sphingomonadales</item><item>Erythrobacteraceae</item><item>Altererythrobacter</item><item>Altererythrobacter marensis</item></taxonLinageNames>");
        let xs = Xml.XStringList("test", xml)
        chai.assert(xs.type === AttributeType.StringList)
        let attribute = xs.data as Model.StringList;
        let items = attribute.items
        chai.assert(items[0]==="cellular organisms")
        chai.assert(items[1]==="Bacteria")
        chai.assert(items[2]==="Proteobacteria")
        chai.assert(items[3]==="Alphaproteobacteria")
        chai.assert(items[4]==="Sphingomonadales")
        chai.assert(items[5]==="Erythrobacteraceae")
        chai.assert(items[6]==="Altererythrobacter")
        chai.assert(items[7]==="Altererythrobacter marensis")
    })
     it("should parse the AccessionList type (XAccessionList)", () => {
        let xml = parse("<taxonLineageIds><accession url=\"http://example.com/\">131567</accession> <accession url=\"http://example.com/\">2</accession> <accession url=\"http://example.com/\">1224</accession> <accession url=\"http://example.com/\">28211</accession> <accession url=\"http://example.com/\">204457</accession> <accession url=\"http://example.com/\">335929</accession> <accession url=\"http://example.com/\">361177</accession> <accession url=\"http://example.com/\">543877</accession></taxonLineageIds>");
        let xs = Xml.XAccessionList("test", xml)
        chai.assert(xs.type === AttributeType.AccessionList)
        let attribute = xs.data as Model.AccessionList;
        let items = attribute.items
        chai.assert(items[0].value === "131567", "wrong value")
        chai.assert(items[0].url === "http://example.com/", "wrong url")
        chai.assert(items[1].value === "2", "wrong value")
        chai.assert(items[1].url === "http://example.com/", "wrong url")
        chai.assert(items[2].value === "1224", "wrong value")
        chai.assert(items[2].url === "http://example.com/", "wrong url")
        chai.assert(items[3].value === "28211", "wrong value")
        chai.assert(items[3].url === "http://example.com/", "wrong url")
        chai.assert(items[4].value === "204457", "wrong value")
        chai.assert(items[4].url === "http://example.com/", "wrong url")
        chai.assert(items[5].value === "335929", "wrong value")
        chai.assert(items[5].url === "http://example.com/", "wrong url")
        chai.assert(items[6].value === "361177", "wrong value")
        chai.assert(items[6].url === "http://example.com/", "wrong url")
        chai.assert(items[7].value === "543877", "wrong value")
        chai.assert(items[7].url === "http://example.com/", "wrong url")
    })
    it("should parse the PublicationPmid type (XPublicationPmid)", () => {
        let xml = parse("<publicationPmid> <url>http://example.com/</url> <accession url=\"http://pubmed.com/\">335929</accession> </publicationPmid>");
         let xs = Xml.XPublicationPmid("test", xml)
        chai.assert(xs.type === AttributeType.PublicationPmid)
        let attribute = xs.data as Model.PublicationPmid;
        let items = attribute.items
        chai.assert(items[0]==="http://example.com/","url wrong")
        let acc = items[1] as Model.Accession
        chai.assert(acc.value==="335929", "Accession value wrong")
        chai.assert(acc.url==="http://pubmed.com/", "Accession url wrong")
    }) 
})
